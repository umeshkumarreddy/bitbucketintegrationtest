package com.trianglelabs.braingames;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.IOnAreaTouchListener;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.sprite.TiledSprite;
import org.andengine.entity.text.Text;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.extension.physics.box2d.util.Vector2Pool;
import org.andengine.input.sensor.acceleration.AccelerationData;
import org.andengine.input.sensor.acceleration.IAccelerationListener;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.ui.activity.SimpleBaseGameActivity;
import org.andengine.util.math.MathUtils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.SensorManager;
import android.os.Vibrator;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
//import com.google.analytics.tracking.android.EasyTracker;
//import com.google.analytics.tracking.android.MapBuilder;

/**
 * (c) 2010 Nicolas Gramlich
 * (c) 2011 Zynga
 *
 * @author Nicolas Gramlich
 * @since 12:14:29 - 30.06.2010
 */
public class BallMathActivity extends SimpleBaseGameActivity implements IAccelerationListener, IOnSceneTouchListener, IOnAreaTouchListener  {
	// ===========================================================
	// Constants
	// ===========================================================
	
	int[][] bigBall = new int[3][5];
	
	
	
	
	//int bigBally[] = {100,100,100,100,200,200,200,200,300,300,300,300,400,400,400,400};
	
	
	
static int level=1;
	private static final int CAMERA_WIDTH = 800;
	private static final int CAMERA_HEIGHT = 480;
	private static  int MAX = 10;
	
	private   int TEMP_MAX = MAX;
	float wrongTime =0.0f;
	float timeOut = 0.0f;
	int timeLimit=120;
	// ===========================================================
	// Fields
	// ===========================================================
	private PhysicsWorld mPhysicsWorld;
	private float mGravityX=100;
	private float mGravityY=100;
	private BitmapTextureAtlas mBitmapTextureAtlas;
	//ITextureRegion faceTextureRegion;
	private Scene mScene;
//	private MediaPlayer mp;
	TiledTextureRegion mTextureRegion;
	 Text elapsedText;
	 Text ques;
	  Text mGameOverText;




	private int levelRange;
	// ===========================================================
	// Constructors
	// ===========================================================




	private int levelRangeMultiply;
	
	private String tempResult;

	// ===========================================================
	// Getter & Setter
	// ===========================================================

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================
	private  static Font mFont;
	private  static Font tFont;
//	private  static Font mFont2;
	
	Random r= new Random();
	
	Queue queue = new LinkedList();
	HashMap hm = new HashMap();
	
	@Override
	public EngineOptions onCreateEngineOptions() {
		//Toast.makeText(this, "Touch the screen to load a completely new BitmapTextureAtlas in a random location with every touch!", Toast.LENGTH_LONG).show();
		
		
		
		
		// EasyTracker easyTracker  = EasyTracker.getInstance(BallMathActivity.this);
		 
	//	 easyTracker.send(MapBuilder.createEvent("BallMath",
		//			String.valueOf(level), String.valueOf(level), 1l).build());
		 TEMP_MAX = 9+level;
			
			if(level == 1){
				levelRange = 10;
				levelRangeMultiply=5;
				timeLimit=30;
			}else if(level == 2){
				
				levelRange = 12;
				levelRangeMultiply=6;
				timeLimit=40;
			}else if(level == 3){
				timeLimit=50;
				levelRange = 15;
				levelRangeMultiply=7;
			}else if(level == 4){
				timeLimit=60;
				levelRange = 20;
				levelRangeMultiply=8;
			}else if(level == 5){
				timeLimit=70;
				levelRange = 25;
				levelRangeMultiply=9;
			}else if(level == 6){
				timeLimit=80;
				levelRange = 30;
				levelRangeMultiply=10;
				
			}else if(level == 7){
				timeLimit=90;
				levelRange = 35;
				levelRangeMultiply=12;
			}else if(level == 8){
				timeLimit=100;
				levelRange = 40;
				levelRangeMultiply=15;
			}else if(level == 9){
				timeLimit=120;
				levelRange = 50;
				levelRangeMultiply=20;
				
			}else if(level == 10){
				timeLimit=130;
				levelRange = 55;
				levelRangeMultiply=25;
				
			}else if(level == 11){
				timeLimit=140;
				levelRange = 60;
				levelRangeMultiply=30;
				
			}else if(level == 12){
				timeLimit=150;
				levelRange = 65;
				levelRangeMultiply=35;
				
			}else if(level == 13){
				timeLimit=160;
				levelRange = 70;
				levelRangeMultiply=40;
				
			}else if(level ==14){
				timeLimit=170;
				levelRange = 75;
				levelRangeMultiply=45;
				
			}else if(level == 15){
				timeLimit=180;
				levelRange = 80;
				levelRangeMultiply=50;
				
			}
		
		
	//	TEMP_MAX=MAX;
		//final Camera camera = new Camera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
		
		Camera camera = new Camera(0.0F, 0.0F, 800F, 480F);
        EngineOptions engineoptions = new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED, new FillResolutionPolicy(), camera);
        engineoptions.getAudioOptions().setNeedsSound(true);
        engineoptions.getAudioOptions().setNeedsMusic(true); 
        return engineoptions;

		//return new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED, new RatioResolutionPolicy(CAMERA_WIDTH, CAMERA_HEIGHT), camera);
	}

	@Override
	public void onCreateResources() {
		/* Nothing done here. */
		
	BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
		
		
		FontFactory.setAssetBasePath("font/");

		final ITexture fontTexture = new BitmapTextureAtlas(this.getTextureManager(), 256, 512, TextureOptions.BILINEAR);
		final ITexture tfontTexture = new BitmapTextureAtlas(this.getTextureManager(), 256, 512, TextureOptions.BILINEAR);
		if(level < 5){
		//this.mFont = FontFactory.createFromAsset(this.getFontManager(), fontTexture, this.getAssets(), "Plok.ttf", 8, true, android.graphics.Color.WHITE);
		this.mFont  =FontFactory.createFromAsset(this.getFontManager(), fontTexture, this.getAssets(), "VniWhimsy.ttf", 24F, true, Color.parseColor("#1A4A61"));
		
		}else{
			
			this.mFont  =FontFactory.createFromAsset(this.getFontManager(), fontTexture, this.getAssets(), "VniWhimsy.ttf", 24F, true, Color.parseColor("#1A4A61"));
		}
		
		this.tFont  =FontFactory.createFromAsset(this.getFontManager(), tfontTexture, this.getAssets(), "VniWhimsy.ttf", 48F, true, Color.WHITE);
		this.tFont.load();
		this.mFont.load();
		//this.mFont = FontFactory.createFromAsset(this.getFontManager(), fontTexture, this.getAssets(), "Plok.ttf", 8, true, android.graphics.Color.WHITE);
//		this.mFont2  =FontFactory.createFromAsset(this.getFontManager(), fontTexture, this.getAssets(), "VniWhimsy.ttf", 48F, true, -256);
//		this.mFont2.load();
	}

	@Override
	public Scene onCreateScene() {
		
		this.mPhysicsWorld = new PhysicsWorld(new Vector2(0, SensorManager.GRAVITY_EARTH), false);
		
		this.mScene = new Scene();
		this.mScene.setBackground(new Background(0.49019f, 0.81568f, 0.71372f));
		this.mScene.setOnSceneTouchListener(this);

		final VertexBufferObjectManager vertexBufferObjectManager = this.getVertexBufferObjectManager();
		final Rectangle ground = new Rectangle(0, CAMERA_HEIGHT - 2, CAMERA_WIDTH, 2, vertexBufferObjectManager);
		final Rectangle roof = new Rectangle(0, 0, CAMERA_WIDTH, 2, vertexBufferObjectManager);
		final Rectangle left = new Rectangle(0, 0, 2, CAMERA_HEIGHT, vertexBufferObjectManager);
		final Rectangle right = new Rectangle(CAMERA_WIDTH - 2, 0, 2, CAMERA_HEIGHT, vertexBufferObjectManager);

		final FixtureDef wallFixtureDef = PhysicsFactory.createFixtureDef(0, 0.5f, 0.5f);
		PhysicsFactory.createBoxBody(this.mPhysicsWorld, ground, BodyType.StaticBody, wallFixtureDef);
		PhysicsFactory.createBoxBody(this.mPhysicsWorld, roof, BodyType.StaticBody, wallFixtureDef);
		PhysicsFactory.createBoxBody(this.mPhysicsWorld, left, BodyType.StaticBody, wallFixtureDef);
		PhysicsFactory.createBoxBody(this.mPhysicsWorld, right, BodyType.StaticBody, wallFixtureDef);

		this.mScene.attachChild(ground);
		this.mScene.attachChild(roof);
		this.mScene.attachChild(left);
		this.mScene.attachChild(right);

		this.mScene.registerUpdateHandler(this.mPhysicsWorld);

		this.mScene.setOnAreaTouchListener(this);
		//this.mEngine.registerUpdateHandler(new FPSLogger());
		
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
		
		if(level < 11){
		this.mBitmapTextureAtlas  = new BitmapTextureAtlas(this.getTextureManager(), 96, 96,TextureOptions.BILINEAR);
	//	this.faceTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTextureAtlas, this, "balloon64.png", 0, 0);
		
	 //   BitmapTextureAtlas bitmaptextureatlas10 = new BitmapTextureAtlas(mBaseGameActivity.getTextureManager(), 96, 96);
		mTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlas , this, "balloon96.png", 0, 0, 1, 1);
       // bitmaptextureatlas10.load();
		}else{
			this.mBitmapTextureAtlas  = new BitmapTextureAtlas(this.getTextureManager(), 64, 64, TextureOptions.BILINEAR);
			//	this.faceTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTextureAtlas, this, "balloon64.png", 0, 0);
				
			 //   BitmapTextureAtlas bitmaptextureatlas10 = new BitmapTextureAtlas(mBaseGameActivity.getTextureManager(), 96, 96);
				mTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(this.mBitmapTextureAtlas , this, "balloon64.png", 0, 0, 1, 1);
		       // bitmaptextureatlas10.load();
			
			
		}
		
        
		this.mBitmapTextureAtlas.load();

		final float x = (CAMERA_WIDTH - mTextureRegion.getWidth()) * MathUtils.RANDOM.nextFloat();
		final float y = (CAMERA_HEIGHT - mTextureRegion.getHeight()) * MathUtils.RANDOM.nextFloat();
	//	final Sprite clickToUnload = new Sprite(x, y, faceTextureRegion, this.getVertexBufferObjectManager());
	//	this.mScene.attachChild(clickToUnload);

	//	this.mScene = new Scene();
	//	this.mScene.setBackground(new Background(0.09804f, 0.6274f, 0.8784f));

//		this.mScene.setOnSceneTouchListener(new IOnSceneTouchListener() {
//			@Override
//			public boolean onSceneTouchEvent(final Scene pScene, final TouchEvent pSceneTouchEvent) {
//				if(pSceneTouchEvent.isActionDown()) {
//					LoadTextureExample.this.loadNewTexture();
//				}
//
//				return true;
//			}
//		});
		
		
	
		
	
		
		
		
			
			for(int i=0;i<9+level;i++){
				
				int one= r.nextInt(levelRange);
				int two= r.nextInt(levelRange);
				int result =0;
				int plusOrMinus= r.nextInt(3);
				if(plusOrMinus == 0){
					 addFace(i*40, i*20.0f, one+"+"+two);
				 result = one+two;
				 hm.put(one+"+"+two, result);
				 queue.add(result);
				}
				else if(plusOrMinus == 1){
					
					 addFace(i*40, i*20.0f, one+"-"+two);
					 result = one-two;
					 hm.put(one+"-"+two, result);
					 queue.add(result);
				}else{
					
					 one= r.nextInt(levelRangeMultiply);
					 two= r.nextInt(levelRangeMultiply);
					
					 addFace(i*40, i*20.0f, one+"X"+two);
					 result = one*two;
					 hm.put(one+"X"+two, result);
					 queue.add(result);
				}
				
			}
			
		
		/*
			
			for(int i=1;i<=MAX;i++){
				 
				if(i <= 5){
					addFace(i*40, 50.0f, i);
				}
				
			     else if(i > 5)
				addFace(i*40, 100.0f, i);

				else if(i >= 10)
					addFace(i*40,200.0f, i);
				
				else if(i >= 15)
					addFace(i*40, 300.0f, i);
				else if(i >= 20)
					addFace(i*40, 400.0f, i);
			
			}
			
		}
		
		else{
		
		for(int i=1;i<=MAX;i++){
			
			
			if(MAX <= 30){
				
				if(i <= 5){
					addFace(i*25, 0.0f, i);
				}
				
			     else if(i > 5)
				addFace(i*25, 75.0f, i);

				else if(i >= 10)
					addFace(i*25, 150.0f, i);
				
				else if(i >= 15)
					addFace(i*25, 225.0f, i);
				else if(i >= 20)
					addFace(i*25, 300.0f, i);
				else if(i >= 25)
					addFace(i*25,350.0f, i);
				
				else if(i >= 30)
					addFace(i*25, 400.0f, i);
				
			}else{
		 
			if(i <= 5){
				addFace(i*16, 0.0f, i);
			}
			
		     else if(i > 5)
			addFace(i*16, 50.0f, i);

			else if(i >= 10)
				addFace(i*16, 100.0f, i);
			
			else if(i >= 15)
				addFace(i*16, 150.0f, i);
			else if(i >= 20)
				addFace(i*16, 200.0f, i);
			else if(i >= 25)
				addFace(i*16,250.0f, i);
			
			else if(i >= 30)
				addFace(i*16, 280.0f, i);
			
			else if(i >= 35)
				addFace(i*16, 320.0f, i);
			else if(i >= 40)
				addFace(i*16, 360.0f, i);
			else if(i >= 45)
				addFace(i*16, 400.0f, i);
			}
		}
		}
		
		*/
		
		 elapsedText = new Text(580, 0, this.tFont, "Time:", "Time: XXXXXXXXXXXXXXXX".length(), this.getVertexBufferObjectManager());
		//final Text fpsText = new Text(250, 240, this.mFont, "FPS:", "FPS: XXXXX".length(), this.getVertexBufferObjectManager());

		this.mScene.attachChild(elapsedText);
		
		 ques = new Text(20, 0, this.tFont, " ", " XXXXXXXXXXXXXXXX".length(), this.getVertexBufferObjectManager());
			//final Text fpsText = new Text(250, 240, this.mFont, "FPS:", "FPS: XXXXX".length(), this.getVertexBufferObjectManager());
		
		 tempResult =String.valueOf(queue.poll());
		 ques.setText(tempResult);
			this.mScene.attachChild(ques);
		
		this.mScene.registerUpdateHandler(new TimerHandler(1 / 20.0f, true, new ITimerCallback() {
			@Override
			public void onTimePassed(final TimerHandler pTimerHandler) {
				//timeOut = LoadTextureExample.this.mEngine.getSecondsElapsedTotal();
				
//				try {
//					Thread.sleep(1000);
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
				
				timeOut =BallMathActivity.this.mEngine.getSecondsElapsedTotal();
				//timeOut = timeOut/1000;
				timeOut = timeOut+wrongTime;
				
				String stime = String.valueOf(timeOut);
				
				stime = stime.substring(0,stime.indexOf('.'));
			//	float f = LoadTextureExample.this.mEngine.getSecondsElapsedTotal()+wrongTime;
				elapsedText.setText("Time: " +(timeLimit-Integer.parseInt(stime)) );
				
				/* The game-over text. */
				if(0 >=(timeLimit-Integer.parseInt(stime))){
					
					BallMathActivity.this.mScene.detachChildren();
					Intent intent = new Intent(BallMathActivity.this, BallMathResultActivity.class);
					BallMathResultActivity.score = "F";
					
					BallMathResultActivity.level=String.valueOf(level);
					 startActivity(intent);
					 BallMathActivity.this.mEngine.stop();
					 finish();
					
//				mGameOverText = new Text(0, 0, mFont2, "Game\nOver", new TextOptions(HorizontalAlign.CENTER), getVertexBufferObjectManager());
//				mGameOverText.setPosition((CAMERA_WIDTH - mGameOverText.getWidth()) * 0.5f, (CAMERA_HEIGHT - mGameOverText.getHeight()) * 0.5f);
//				mGameOverText.registerEntityModifier(new ScaleModifier(3, 0.1f, 2.0f));
//				mGameOverText.registerEntityModifier(new RotationModifier(3, 0, 720));
//				   mScene.attachChild(mGameOverText);
				}
				//wrongTime = 0.0f;
			}
		}));
	
//		
//		this.mScene.registerUpdateHandler(new IUpdateHandler() {
//			
//			@Override
//			public void reset() {
//				// TODO Auto-generated method stub
//				
//			}
//			
//			@Override
//			public void onUpdate(float pSecondsElapsed) {
//				// TODO Auto-generated method stub
//				elapsedText.setText("Time: " + pSecondsElapsed);
//			}
//		});
		
//		Thread thr = new Thread() {
//
//			  @Override
//			  public void run() {
//		          try {
//		        	 
//		          
//		                  
//		                    	  for(int i = 0; i< 120; i++){
//		              				
//		              				try {
//		              					Thread.sleep(1000);
//		              				} catch (InterruptedException e) {
//		              					// TODO Auto-generated catch block
//		              					e.printStackTrace();
//		              				}
//		              					time=i;
//		              			
//		                    	  }	
//		                  
//		                 
//		              
//		          } catch (Exception e) {
//		          }
//		      }
//		  };
//		 thr.start();
//		Mythread t = new Mythread();
//        t.run();

//		this.mScene.registerUpdateHandler(new TimerHandler(1 / 20.0f, true, new ITimerCallback() {
//			@Override
//			public void onTimePassed(final TimerHandler pTimerHandler) {
//				elapsedText.setText("Seconds elapsed: " + LoadTextureExample.this.mEngine.getSecondsElapsedTotal());
//				
//			}
//		}));
		
	//	addFace(100f, 100f);
		return this.mScene;
	}

	// ===========================================================
	// Methods
	// ===========================================================
	private int mFaceCount = 0;
	
	private void addFace(final float pX, final float pY , String string) {
		this.mFaceCount++;

		final Sprite face;
		final Body body;

		final FixtureDef objectFixtureDef = PhysicsFactory.createFixtureDef(1, 0.5f, 0.5f);

	//	if(this.mFaceCount % 2 == 1){
			face = new TiledSprite(-64F, -64F, this.mTextureRegion, this.getVertexBufferObjectManager());
			//mp = MediaPlayer.create(this, R.raw.touch);
			//  mp.start();
			  
			  System.out.println("Face px --->"+pX+"  "+pY);
				face.setPosition(pX,pY);
//			 if(number>25){
//					face.setPosition(400.0f,400.0f);
//			}
//			else if(number>20){
//					face.setPosition(300.0f,300.0f);
//			}
//			else if(number>15){
//				face.setPosition(250.0f,250.0f);
//			}else if(number>10){
//				face.setPosition(200.0f,200.0f);
//			}else 	if(number>5){
//				face.setPosition(100.0f,50.0f);
//			}else{
//			face.setPosition(0.0f,0.0f);
//			}
			Text mText = new Text(0.0F, 0.0F, mFont, "", 8, this.getVertexBufferObjectManager());
			//mText.setHeight(0.0f);
			  mText.setPosition((face.getWidth()-mText.getHeight()-15)/2.0F, (face.getHeight() - mText.getHeight()) / 2.0F);
				face.attachChild(mText);
				mText.setText(String.valueOf(string));
				//face.setCurrentTileIndex(0);
				
			body = PhysicsFactory.createBoxBody(this.mPhysicsWorld, face, BodyType.DynamicBody, objectFixtureDef);
//		} else {
//			face = new AnimatedSprite(pX, pY, this.mCircleFaceTextureRegion, this.getVertexBufferObjectManager());
//			body = PhysicsFactory.createCircleBody(this.mPhysicsWorld, face, BodyType.DynamicBody, objectFixtureDef);
//		}

		this.mPhysicsWorld.registerPhysicsConnector(new PhysicsConnector(face, body, true, true));

		//face.animate(new long[]{200,200}, 0, 1, true);
		face.setUserData(body);
		this.mScene.registerTouchArea(face);
		this.mScene.attachChild(face);
	}
	
	private void loadNewTexture() {
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");

		this.mBitmapTextureAtlas  = new BitmapTextureAtlas(this.getTextureManager(), 32, 32, TextureOptions.BILINEAR);
		final ITextureRegion faceTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(this.mBitmapTextureAtlas, this, "balloon64.png", 0, 0);
		this.mBitmapTextureAtlas.load();

		final float x = (CAMERA_WIDTH - faceTextureRegion.getWidth()) * MathUtils.RANDOM.nextFloat();
		final float y = (CAMERA_HEIGHT - faceTextureRegion.getHeight()) * MathUtils.RANDOM.nextFloat();
		final Sprite clickToUnload = new Sprite(x, y, faceTextureRegion, this.getVertexBufferObjectManager());
		this.mScene.attachChild(clickToUnload);
	}
int time;
	@Override
	public boolean onAreaTouched(TouchEvent pSceneTouchEvent,
			ITouchArea pTouchArea, float pTouchAreaLocalX,
			float pTouchAreaLocalY) {
		
		
		if(pSceneTouchEvent.isActionDown()) {
			final Sprite face = (Sprite) pTouchArea;
		
			final Body faceBody = (Body)face.getUserData();

			final Vector2 velocity = Vector2Pool.obtain(this.mGravityX * -1850, this.mGravityY * 1550);
			faceBody.setLinearVelocity(velocity);
			Vector2Pool.recycle(velocity);
			
		//	mp = MediaPlayer.create(this, R.raw.touch);
			//  mp.start();
			  
			  Text mText = (Text)face.getChildByIndex(0);
			  
			 if(null != hm.get(mText.getText().toString()) &&  String.valueOf(hm.get(mText.getText().toString())).equals(tempResult)) {
			  
				this.mScene.detachChild(face);
				TEMP_MAX--;
				tempResult =String.valueOf(queue.poll());
				 ques.setText(tempResult);
				if(TEMP_MAX==0){
					
					BallMathActivity.this.mScene.detachChildren();
				Intent intent = new Intent(this, BallMathResultActivity.class);
				BallMathResultActivity.score = "T";
				
				BallMathResultActivity.level=String.valueOf(level);
				 startActivity(intent);
				 BallMathActivity.this.mEngine.stop();
				 finish();
				}
			 }
			 else{
				 
				 Vibrator v = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);
  				 // Vibrate for 500 milliseconds
  				 v.vibrate(500);
				 wrongTime = wrongTime+5.0f;
			 }
		//	elapsedText.setText("Time: " + time);
			return true;
		}

		return false;
		
		
	}

	@Override
	public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onAccelerationAccuracyChanged(AccelerationData pAccelerationData) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAccelerationChanged(AccelerationData pAccelerationData) {
		// TODO Auto-generated method stub
		this.mGravityX = pAccelerationData.getX();
		this.mGravityY = pAccelerationData.getY();

		final Vector2 gravity = Vector2Pool.obtain(this.mGravityX, this.mGravityY);
		this.mPhysicsWorld.setGravity(gravity);
		Vector2Pool.recycle(gravity);
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

		
	        	
		BallMathActivity.this.mScene.detachChildren();
		 Intent intent = new Intent(this, BallMathMenuActivity.class);
		 startActivity(intent);
		 finish();
	       System.exit(0);
	       
	            //close();


	    
	}
	
	@Override
	protected synchronized void onResume() {
		
		try{
			
			super.onResume();
		}catch(Exception e){
			
		}
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}
	class Mythread implements Runnable{

		@Override
		public void run() {
			// TODO Auto-generated method stub
			
			for(int i = 0; i< 60; i++){
				
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				elapsedText.setText("Seconds elapsed: " + i);
			}
			
		}

		
		
		
	}

	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================
}
