package com.trianglelabs.braingames;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Map;

//import com.google.analytics.tracking.android.EasyTracker;
//import com.google.analytics.tracking.android.MapBuilder;
import com.yrkfgo.assxqx4.AdConfig;
import com.yrkfgo.assxqx4.AdView;
import com.yrkfgo.assxqx4.Main;
import com.yrkfgo.assxqx4.AdConfig.AdType;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class BallMathResultActivity extends Activity {

	public static String score;
	public static String scoreV;
	public static String level;
	Context context;
	//private EasyTracker easyTracker = null;
	private Main main; //Declare here
	AdView adView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ball_math_result);
		Button pAgain = (Button) findViewById(R.id.pAgain);
		try{
			
			try{
				try{
					
					
				AirPushAds.main.showCachedAd(AdType.overlay);
				}catch(Exception e){
									
								}
				AdConfig.setAppId(295645);
		   		AdConfig.setApiKey("1450682996266650273");
		  
		   		AdConfig.setCachingEnabled(true);
				
		   		adView = (AdView) findViewById(R.id.myAdView);
				adView.setBannerType(AdView.BANNER_TYPE_IN_APP_AD);
				adView.setBannerAnimation(AdView.ANIMATION_TYPE_FADE);
				adView.showMRinInApp(false);
				
		   //initialize Airpush
		      main=new Main(this); //Here this is reference of current Activity. 

		      main.startInterstitialAd(AdType.overlay);
		      AirPushAds.main = main;
				}catch(Exception e){
					
				}
		
//			easyTracker = EasyTracker.getInstance(BallMathResultActivity.this);
//
//			easyTracker.send(MapBuilder.createEvent("BallMathResult",
//					scoreV, level, 1l).build());
			
//			Typeface tf = Typeface.createFromAsset(getAssets(),
//			        "fonts/Kleymissky_0283.otf");

	        FileInputStream fis = openFileInput("ballmath");
	        ObjectInputStream ois = new ObjectInputStream(fis);
	        Map anotherMap = (Map) ois.readObject();
	        
	        anotherMap.put("level"+level,scoreV );
	        
	        ois.close();
        
		context = this;
		TextView	timerValue = (TextView) findViewById(R.id.res);
		TextView	allTheBest = (TextView) findViewById(R.id.allTheBest);
	//	timerValue.setTypeface(tf);
		String res = "";
		 ImageView win = (ImageView) findViewById(R.id.win);
		  ImageView gameOver = (ImageView) findViewById(R.id.gameOver);
		if(score.equalsIgnoreCase("T")){
			win.setVisibility(win.VISIBLE);
			gameOver.setVisibility(gameOver.GONE);
			pAgain.setVisibility(pAgain.GONE);
			  Button nextLevel = (Button) findViewById(R.id.nextLevel);
			  nextLevel.setVisibility(pAgain.VISIBLE);
			if((Integer.parseInt(level) == 14)){
				res = " Congratulations... You finised all the levels. Great job";
				nextLevel.setText("Level "+(Integer.parseInt(level)+1));
				
			}else{
			
			res = " You finised this level.Next level has been unlocked.";
			nextLevel.setText("Level "+(Integer.parseInt(level)+1));
			}
			  anotherMap.put("levelCompleted"+(Integer.parseInt(level)+1),"Y" );
			  
			  nextLevel.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						BallMathActivity.level = (Integer.parseInt(level)+1);
						 Intent intent = new Intent(context, BallMathActivity.class);
							score = null;
						 startActivity(intent);
						 finish();
					}
				});
		}else{
			win.setVisibility(win.GONE);
			gameOver.setVisibility(gameOver.VISIBLE);
			
			res = "You could not finish this level. Try again.";
			
			allTheBest.setVisibility(allTheBest.VISIBLE);
			pAgain.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					 Intent intent = new Intent(context, BallMathActivity.class);
						score = null;
					 startActivity(intent);
					 finish();
				}
			});
			
		}
		
		timerValue.setText(res);
		
		
		Button menu = (Button) findViewById(R.id.menu);
		menu.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				 Intent intent = new Intent(context, BallMathMenuActivity.class);
					score = null;
				 startActivity(intent);
				 finish();
			}
		});
		
	     FileOutputStream fos = openFileOutput("ballmath", Context.MODE_PRIVATE);
	        ObjectOutputStream oos = new ObjectOutputStream(fos);
	        oos.writeObject(anotherMap);
	        oos.close();
	        
		}catch(Exception e){
			
			e.printStackTrace();
		}
        
        
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		 Intent intent = new Intent(context, BallMathMenuActivity.class);
			score = null;
		 startActivity(intent);
		 finish();
		
	}
	
	

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.result, menu);
//		return true;
//	}

}
