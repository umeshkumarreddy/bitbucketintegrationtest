package com.trianglelabs.braingames;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

/**
 * Created by UmeshMaxx on 21-07-2016.
 */
public class BrainVsBrainHowToPlayActivity extends Activity{

    private Button buttonPlay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_brain_vs_brain);

        buttonPlay = (Button) findViewById(R.id.button_play_bvb);

        buttonPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BrainVsBrainHowToPlayActivity.this,));
            }
        });

    }
}
