package com.trianglelabs.braingames;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

import com.nhaarman.listviewanimations.itemmanipulation.DynamicListView;
import com.nhaarman.listviewanimations.itemmanipulation.dragdrop.TouchViewDraggableManager;

import com.yrkfgo.assxqx4.AdConfig;
import com.yrkfgo.assxqx4.AdView;
import com.yrkfgo.assxqx4.Main;
import com.yrkfgo.assxqx4.AdConfig.AdType;

public class ColorListActivity extends ActionBarActivity implements OnClickListener {

	public static final String LIST_VIEW_OPTION = "com.csform.android.uiapptemplate.DragAndDropShopActivity";

	private DynamicListView mDynamicListView;
	static int [] colorPick;
	Button validated ;
	LinearLayout colorLayout;
	StringBuffer sb = new StringBuffer();
	static int listSize = 3;
	private Main main; //Declare here
	AdView adView;
	Context context;
	static ArrayList<DummyModel> list = new ArrayList<DummyModel>();
	ArrayList<String> colList = new ArrayList<String>();
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = this;
		AdConfig.setAppId(295645);
   		AdConfig.setApiKey("1450682996266650273");
  
   		AdConfig.setCachingEnabled(true);
     
 
	//for calling banner 360
   		//pass the current Activity's reference.
   	
   		setContentView(R.layout.brain_color_activity);
   		adView = (AdView) findViewById(R.id.myAdView);
			adView.setBannerType(AdView.BANNER_TYPE_IN_APP_AD);
			adView.setBannerAnimation(AdView.ANIMATION_TYPE_FADE);
			adView.showMRinInApp(false);
			
	   //initialize Airpush
	      main=new Main(this); //Here this is reference of current Activity. 

	      main.startInterstitialAd(AdType.interstitial);
	      AirPushAds.main = main;
	      
	 
		colList.add("#303F9F");
		colList.add("#E91E63");
		colList.add("#FFC107");
		colList.add("#FF5722");
		colList.add("#F44336");
		colList.add("#CDDC39");
		colList.add("#5D4037");
		colList.add("#607D8B");
		colList.add("#7B1FA2");
		colList.add("#00BCD4");
		
		
//		TextView orderNumber = (TextView) findViewById(R.id.activity_list_view_drag_and_drop_shop_order_number);
//		TextView date = (TextView) findViewById(R.id.activity_list_view_drag_and_drop_shop_date);
//		TextView price = (TextView) findViewById(R.id.activity_list_view_drag_and_drop_shop_price);
//		TextView proceed = (TextView) findViewById(R.id.activity_list_view_drag_and_drop_shop_proceed);
		
	//	proceed.setOnClickListener(this);
		 validated = (Button) findViewById(R.id.validate);
		 validated.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ColorListAdapter.mDummyModelList.equals(ColorListAdapter.mDummyModelList);
				int[] colInt = new int[ColorListAdapter.mDummyModelList.size()];
				int x=0;
				StringBuffer sb2 = new StringBuffer();
				for(DummyModel col : ColorListAdapter.mDummyModelList){
					
					sb2.append(col.getText());
					
					
					
					
				}
				
				
				if(sb.toString().equals(sb2.toString())){
					Toast.makeText(v.getContext(), "Correct",
							Toast.LENGTH_SHORT).show();
					ColorResultActivity.score= "T";
					
					Intent intent = new Intent(context, ColorResultActivity.class);

					startActivity(intent);
					finish();
					return;
				}else{
					Toast.makeText(v.getContext(), "Wrong",
							Toast.LENGTH_SHORT).show();
					ColorResultActivity.score= "F";
					Intent intent = new Intent(context, ColorResultActivity.class);

					startActivity(intent);
					finish();
					return;
				}
			}
		});
		// colorLayout = (LinearLayout) findViewById(R.id.colorsLayout);
		
//		View color1 = (View) findViewById(R.id.color1);
//		View color2 = (View) findViewById(R.id.color2);
//		View color3 = (View) findViewById(R.id.color3);
//		View color4 = (View) findViewById(R.id.color4);
//		View color5 = (View) findViewById(R.id.color5);
//		View color6 = (View) findViewById(R.id.color6);
//		View color7 = (View) findViewById(R.id.color7);
//		View color8 = (View) findViewById(R.id.color8);
//		View color9 = (View) findViewById(R.id.color9);
//		View color10 = (View) findViewById(R.id.color10);
		list = new ArrayList<DummyModel>();
		
		colorPick = new int[listSize];
		
		Random r= new Random();
		for(int i=0;i<listSize;i++){
		   int j=	r.nextInt(9);
		  if( checkForDuplicate(j+1)){
			  i--;
			  continue;
		  }
		   colorPick[i] = j+1;
		   
		   
		   
		     if(j+1 == 1){
			   //color1.setVisibility(color1.VISIBLE);   
				list.add(new DummyModel(i, "http://pengaja.com/uiapptemplate/newphotos/shop/0.jpg", "#303F9F",1 ));
				sb.append("#303F9F");
		    }else  if(j+1 == 2){
		    	//color2.setVisibility(color2.VISIBLE);
		    	list.add(new DummyModel(i, "http://pengaja.com/uiapptemplate/newphotos/shop/0.jpg", "#E91E63",1 ));
		    	sb.append("#E91E63");
		    }else  if(j+1 == 3){
		    	sb.append("#FFC107");
		    	//color3.setVisibility(color3.VISIBLE);
		    	list.add(new DummyModel(i, "http://pengaja.com/uiapptemplate/newphotos/shop/0.jpg", "#FFC107",1 ));
		    }else  if(j+1 == 4){
		    	sb.append("#FF5722");
		    	//color4.setVisibility(color4.VISIBLE);
		    	list.add(new DummyModel(i, "http://pengaja.com/uiapptemplate/newphotos/shop/0.jpg", "#FF5722",1 ));
		    }else  if(j+1 == 5){
		    	sb.append("#F44336");
		    	//color5.setVisibility(color5.VISIBLE);
		    	list.add(new DummyModel(i, "http://pengaja.com/uiapptemplate/newphotos/shop/0.jpg", "#F44336",1 ));
		    }else  if(j+1 == 6){
		    	sb.append("#CDDC39");
		    	//color6.setVisibility(color6.VISIBLE);
		    	list.add(new DummyModel(i, "http://pengaja.com/uiapptemplate/newphotos/shop/0.jpg", "#CDDC39",1 ));
		    }else  if(j+1 == 7){
		    	sb.append("#5D4037");
		    	//color7.setVisibility(color7.VISIBLE);
		    	list.add(new DummyModel(i, "http://pengaja.com/uiapptemplate/newphotos/shop/0.jpg", "#5D4037",1 ));
		    }else  if(j+1 == 8){
		    	sb.append("#607D8B");
		    	//color8.setVisibility(color8.VISIBLE);
		    	list.add(new DummyModel(i, "http://pengaja.com/uiapptemplate/newphotos/shop/0.jpg", "#607D8B",1 ));
		    }else  if(j+1 == 9){
		    	sb.append("#7B1FA2");
		    	list.add(new DummyModel(i, "http://pengaja.com/uiapptemplate/newphotos/shop/0.jpg", "#7B1FA2",1 ));
		    	//color9.setVisibility(color9.VISIBLE);
		    }else  if(j+1 == 10){
		    	sb.append("#00BCD4");
		    	list.add(new DummyModel(j, "http://pengaja.com/uiapptemplate/newphotos/shop/0.jpg", "#00BCD4",1 ));
		    //	color10.setVisibility(color10.VISIBLE);
		    }
			
		}
		
		
		
		
		
		mDynamicListView = (DynamicListView) findViewById(R.id.activity_list_view_drag_and_drop_dynamic_listview);
		mDynamicListView.setDividerHeight(0);
		setUpDragAndDrop(false);
		hand1.postDelayed(run, (long) (listSize*1.5*1000));
		
		

//		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//		getSupportActionBar().setTitle("Drag and Drop Shop");
	}

	private boolean checkForDuplicate(int j) {
		// TODO Auto-generated method stub
		for(int i=0;i<colorPick.length;i++){
			
			if(j==colorPick[i]){
				return true;
			}
		}
		
		return false;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public static ArrayList<DummyModel> getDummyModelDragAndDropShopList() {
		

	//	for(int i=0;i<colorPick.length;i++){
		
//		list.add(new DummyModel(0, "http://pengaja.com/uiapptemplate/newphotos/shop/0.jpg", "#CC4D41",1 ));
//		list.add(new DummyModel(1, "http://pengaja.com/uiapptemplate/newphotos/shop/0.jpg", "#CA9A32",1 ));
//		list.add(new DummyModel(2, "http://pengaja.com/uiapptemplate/newphotos/shop/0.jpg", "#6C8EB2",1 ));
		//}
		
		return list;
	}

	private void setUpDragAndDrop(boolean flag) {
		
		try{
		
		final ColorListAdapter adapter = new ColorListAdapter(this,
				getDummyModelDragAndDropShopList(),colorPick,flag);
		mDynamicListView.setAdapter(adapter);
		mDynamicListView.enableDragAndDrop();
		TouchViewDraggableManager tvdm = new TouchViewDraggableManager(
				R.id.icon);
		mDynamicListView.setDraggableManager(tvdm);
		mDynamicListView
				.setOnItemLongClickListener(new OnItemLongClickListener() {

					@Override
					public boolean onItemLongClick(AdapterView<?> parent,
							View view, int position, long id) {
						mDynamicListView.startDragging(position);
						return true;
					}
				});
		}catch(Exception e){
			
		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		try{
		super.onBackPressed();
		 Intent intent = new Intent(context, MegaMenuActivity.class);
			
		 startActivity(intent);
		
		 finish();
		}catch(Exception e){}
	}
	

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Toast.makeText(this, "Proceed...", Toast.LENGTH_SHORT).show();
	}
	
	Runnable run = new Runnable() {
		@Override
		public void run() {
			validated.setVisibility(validated.VISIBLE);
			//colorLayout.setVisibility(validated.GONE);
			setUpDragAndDrop(true);
			Toast.makeText(context, "Long press an item to start dragging",
					Toast.LENGTH_SHORT).show();
		}
	};
	
	Handler hand1 = new Handler();
}
