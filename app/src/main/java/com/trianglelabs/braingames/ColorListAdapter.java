package com.trianglelabs.braingames;

import java.util.ArrayList;
import java.util.Collections;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.nhaarman.listviewanimations.util.Swappable;

public class ColorListAdapter extends BaseAdapter implements Swappable {

	private Context mContext;
	private LayoutInflater mInflater;
	static public ArrayList<DummyModel> mDummyModelList;
	static public ArrayList<DummyModel> orgDummyModelList;
	ArrayList<String> colList = new ArrayList<String>();
	
	int[] colorPick;
	static int[] OrgColorPick;
	public ColorListAdapter(Context context,
			ArrayList<DummyModel> dummyModelList,int[] colorPick,boolean flag) {
		
		colList.add("#303F9F");
		colList.add("#E91E63");
		colList.add("#FFC107");
		colList.add("#FF5722");
		colList.add("#F44336");
		colList.add("#CDDC39");
		colList.add("#5D4037");
		colList.add("#607D8B");
		colList.add("#7B1FA2");
		colList.add("#00BCD4");
		mContext = context;
		mInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		mDummyModelList = dummyModelList;
		
		
		OrgColorPick= new int[mDummyModelList.size()];
		int x=0;
		for(DummyModel col : mDummyModelList){
			
			OrgColorPick[x] = colList.indexOf(col.getText())+1;
			x++;
			
		}
		
		orgDummyModelList= (ArrayList<DummyModel>) dummyModelList.clone();
		if(flag)
		Collections.shuffle(mDummyModelList);
		this.colorPick = colorPick;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public int getCount() {
		return mDummyModelList.size();
	}

	@Override
	public Object getItem(int position) {
		return mDummyModelList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return mDummyModelList.get(position).getId();
	}
boolean swap = false;
	@Override
	public void swapItems(int positionOne, int positionTwo) {
		swap = true;
		Collections.swap(mDummyModelList, positionOne, positionTwo);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(
					R.layout.color_list, parent, false);
			holder = new ViewHolder();
			
			holder.productName = (TextView) convertView
					.findViewById(R.id.list_item_drag_and_drop_shop_product_name);
		
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		DummyModel dm = mDummyModelList.get(position);
		
		
		//holder.productName.setTextColor(R.color.mathLevel);
//		holder.productName.setText(dm.getText());
//		holder.productName.setBackgroundColor(Color.RED);
//		
//		if(position ==2){
//			holder.productName.setTextColor(R.color.multiNonCompleted);
//		}
		
	 	if(position == 0)
		holder.productName.setBackgroundColor(Color.parseColor(dm.getText()));
	else 	if(position == 1)
		holder.productName.setBackgroundColor(Color.parseColor(dm.getText()));
	else 	if(position == 2)
		holder.productName.setBackgroundColor(Color.parseColor(dm.getText()));
	else 	if(position == 3)
		holder.productName.setBackgroundColor(Color.parseColor(dm.getText()));
	else 	if(position == 4)
		holder.productName.setBackgroundColor(Color.parseColor(dm.getText()));
	else 	if(position == 5)
		holder.productName.setBackgroundColor(Color.parseColor(dm.getText()));
	else 	if(position == 6)
		holder.productName.setBackgroundColor(Color.parseColor(dm.getText()));
	else 	if(position == 7)
		holder.productName.setBackgroundColor(Color.parseColor(dm.getText()));
	else 	if(position == 8)
		holder.productName.setBackgroundColor(Color.parseColor(dm.getText()));
		
		return convertView;
	}

	private static class ViewHolder {
		public ImageView image;
		public/* Roboto */TextView productName;
		public/* Roboto */TextView oldPrice;
		public/* Roboto */TextView price;
		public/* Roboto */TextView sellerName;
		public/* Roboto */TextView shipping;
		public/* Material */TextView icon;
	}
}
