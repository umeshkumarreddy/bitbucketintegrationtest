package com.trianglelabs.braingames;



import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class ColorMainActivity extends ActionBarActivity {

	private Context context;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.color_main_activity);

		Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);

	      setSupportActionBar(mToolbar);
	      mToolbar.setTitle("Colors Vs Brain");
		context = this;
		Button color = (Button) findViewById(R.id.start);
		color.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, ColorListActivity.class);

				startActivity(intent);
				finish();
				return;
			}
		});
	}
	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		try{
		super.onBackPressed();
		 Intent intent = new Intent(context, MegaMenuActivity.class);
			
		 startActivity(intent);
		
		 finish();
		}catch(Exception e){}
	}
	
	
}
