package com.trianglelabs.braingames;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;

//import com.google.analytics.tracking.android.EasyTracker;
//import com.google.analytics.tracking.android.MapBuilder;
//import com.raghu.brain.braingames.R;
import com.yrkfgo.assxqx4.AdConfig;
import com.yrkfgo.assxqx4.AdView;
import com.yrkfgo.assxqx4.Main;
import com.yrkfgo.assxqx4.AdConfig.AdType;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ColorResultActivity extends Activity {

	public static String score;
	public static String scoreV;
	public static int level;
	Context context;
//	private EasyTracker easyTracker = null;
	
	private Main main; //Declare here
	AdView adView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		level = ColorListActivity.listSize-2;
		setContentView(R.layout.color_result);
		Button pAgain = (Button) findViewById(R.id.pAgain);
		try{
		
				
				try{
					try{
						
						
					AirPushAds.main.showCachedAd(AdType.interstitial);
					}catch(Exception e){
										
									}
					AdConfig.setAppId(295645);
			   		AdConfig.setApiKey("1450682996266650273");
			  
			   		AdConfig.setCachingEnabled(true);
					
			   		adView = (AdView) findViewById(R.id.myAdView);
					adView.setBannerType(AdView.BANNER_TYPE_IN_APP_AD);
					adView.setBannerAnimation(AdView.ANIMATION_TYPE_FADE);
					adView.showMRinInApp(false);
					
			   //initialize Airpush
			      main=new Main(this); //Here this is reference of current Activity. 

			      main.startInterstitialAd(AdType.interstitial);
			      AirPushAds.main = main;
					}catch(Exception e){
						
					}
//			easyTracker = EasyTracker.getInstance(ColorResultActivity.this);
//
//			easyTracker.send(MapBuilder.createEvent("ColorResultActivity",
//					String.valueOf(level), String.valueOf(level), 1l).build());
			
			Typeface tf = Typeface.createFromAsset(getAssets(),
			        "font/MaterialDesignIcons.ttf");

	     
        
		context = this;
		Map anotherMap = new HashMap();
try{

	        FileInputStream fis = openFileInput("math");
	        ObjectInputStream ois = new ObjectInputStream(fis);
	         anotherMap = (Map) ois.readObject();
	        
	         if(null != anotherMap.get("color") && level > (Integer) anotherMap.get("color")){
	        	 
	        	 anotherMap.put("color", level);
	         }else  if(null == anotherMap.get("color")){
	        	 anotherMap.put("color", level);
	         }
	         
	         
	        ois.close();
	        ObjectOutputStream oos;
	        FileOutputStream   fos = openFileOutput("math", Context.MODE_PRIVATE);;
			oos = new ObjectOutputStream(fos);
		
			// TODO Auto-generated catch block
        oos.writeObject(anotherMap);
        oos.close();
		}catch(Exception e){
			
			FileOutputStream fos;
			try {
				fos = openFileOutput("math", Context.MODE_PRIVATE);;
			
	        ObjectOutputStream oos;
		
				oos = new ObjectOutputStream(fos);
			
				// TODO Auto-generated catch block
			if(null == anotherMap.get("color"))
				anotherMap.put("color", level);
	        oos.writeObject(anotherMap);
	        oos.close();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				
			}
			
		}
		TextView	timerValue = (TextView) findViewById(R.id.res);
		timerValue.setTypeface(tf);
		String res = "";
		 ImageView win = (ImageView) findViewById(R.id.win);
		  ImageView gameOver = (ImageView) findViewById(R.id.gameOver);
		  
		if(score.equalsIgnoreCase("T")){
			ColorListActivity.listSize = ColorListActivity.listSize+1;
			win.setVisibility(win.VISIBLE);
			gameOver.setVisibility(gameOver.GONE);
			
			pAgain.setVisibility(pAgain.GONE);
			  Button nextLevel = (Button) findViewById(R.id.nextLevel);
			  nextLevel.setVisibility(pAgain.VISIBLE);
			if(level == 9){
				res = " Congratulations... You finised all the levels. Great job";
				nextLevel.setText("Level "+(level));
				
			}else{
			
			res = " Congratulations... You finised this level. Play Next level .";
			level = level+1;
			nextLevel.setText("Level "+(level));
			}
			//  anotherMap.put("levelCompleted"+((level)+1),"Y" );
			  
			  nextLevel.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						//LoadTextureExample.level = ((level)+1);
						 Intent intent = new Intent(context, ColorListActivity.class);
							score = null;
						 startActivity(intent);
						 finish();
					}
				});
		}else{
			
			win.setVisibility(win.GONE);
			gameOver.setVisibility(gameOver.VISIBLE);
			res = "  Game Over... You could not finish this level. Try again.. All the best.";
			
			
			pAgain.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
				//	ColorListActivity.listSize = ColorListActivity.listSize+1;
					 Intent intent = new Intent(context, ColorListActivity.class);
						score = null;
					 startActivity(intent);
					 finish();
				}
			});
			
		}
		
		timerValue.setText(res);
		
//		
		Button menu = (Button) findViewById(R.id.menu);
		menu.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				 Intent intent = new Intent(context, MegaMenuActivity.class);
					score = null;
					ColorListActivity.listSize = 3;
				 startActivity(intent);
				 finish();
			}
		});
//		
	     
	        
		}catch(Exception e){
			
			e.printStackTrace();
		}
        
        
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		ColorListActivity.listSize = 3;
		score = null;
		 Intent intent = new Intent(context, ColorMainActivity.class);

		 startActivity(intent);
		 finish();
	}
	
	

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.result, menu);
//		return true;
//	}

}
