package com.trianglelabs.braingames;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.os.Vibrator;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

//import com.google.analytics.tracking.android.EasyTracker;
//import com.raghu.brain.braingames.R;
import com.yrkfgo.assxqx4.AdConfig;
import com.yrkfgo.assxqx4.AdView;
import com.yrkfgo.assxqx4.Main;
import com.yrkfgo.assxqx4.AdConfig.AdType;

public class FocusActivity extends Activity {

	boolean isThreadRun = false;
	boolean isClicked = false;
	TextView focus;
	TextView focusInfo;
	int random ;
	int focusNo;
	Button start;
	TextView focusno1;
	int chances1 = 3;
	long timeInMilliseconds = 0L;
	long timeSwapBuff = 0L;
	long updatedTime = 0L;
	 int rTemp=0;
	private long startTime = 0L;
	LinearLayout layout1; 
	//private EasyTracker easyTracker = null;
	private int sleepTime= 1500;
	private Handler customHandler = new Handler();
	 LinearLayout fLayout;
	 TextView best;
	 Context context ;
	 
	 private Main main; //Declare here
		AdView adView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Typeface tf = Typeface.createFromAsset(getAssets(),
		        "font/VniWhimsy.ttf");
		context = this;
		Random r= new Random();
		setContentView(R.layout.activity_focus);
		
		AdConfig.setAppId(295645);
   		AdConfig.setApiKey("1450682996266650273");
  
   		AdConfig.setCachingEnabled(true);
     
 
	//for calling banner 360
   		//pass the current Activity's reference.
   	
   	
   		adView = (AdView) findViewById(R.id.myAdView);
			adView.setBannerType(AdView.BANNER_TYPE_IN_APP_AD);
			adView.setBannerAnimation(AdView.ANIMATION_TYPE_FADE);
			adView.showMRinInApp(false);
			
	   //initialize Airpush
	      main=new Main(this); //Here this is reference of current Activity. 

	      main.startInterstitialAd(AdType.interstitial);
	      AirPushAds.main = main;
		focus = (TextView) findViewById(R.id.focus);
		focusInfo = (TextView) findViewById(R.id.focusInfo);
		 focusno1 = (TextView) findViewById(R.id.focusno);
		focusNo =  r.nextInt(9);

		  fLayout = (LinearLayout) findViewById(R.id.focusLayout);
 start = (Button) findViewById(R.id.focusStart);

	layout1 = (LinearLayout) findViewById(R.id.linearLayout1);
focusno1.setText(String.valueOf(focusNo)); 


Map anotherMap = new HashMap();
try{
	
	
	
    FileInputStream fis = openFileInput("math");
    ObjectInputStream ois = new ObjectInputStream(fis);
     anotherMap = (Map) ois.readObject();
    
     if(null != anotherMap.get("focus") ){
    	 
    	 TextView best = (TextView) findViewById(R.id.highScore);
    	 best.setTypeface(tf);
    	 best.setText("BEST:"+anotherMap.get("focus"));
     }
     
     
    ois.close();
}catch(Exception e){
	
}

start.setOnClickListener(new OnClickListener() {
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		//focusno1.setText("");
		 //focusno1.setVisibility(Button.GONE);
		
		 findViewById(R.id.focusBox).setVisibility(Button.GONE);
		 TextView best = (TextView) findViewById(R.id.highScore);
		 //best.setVisibility(Button.GONE);
		 startTime = SystemClock.uptimeMillis();
		customHandler.postDelayed(updateTimerThread, 0);
		focusInfo.setVisibility(Button.GONE);
		start.setVisibility(Button.GONE);
		focus.setVisibility(Button.VISIBLE);
		Thread thr = new Thread() {

			  @Override
			  public void run() {
		          try {
		        	 
		              while (!isInterrupted() && !isThreadRun) {
		            		
		            	  
		            	  runOnUiThread(new Runnable() {
		                      
		            		
		            		  
		            		  @Override
		                      public void run() {
		                    
		                    	  fLayout.setBackgroundColor(Color.BLACK);
		                    	  Random r= new Random();
		                    	 
		                    	  while(true){
		                    		  random = r.nextInt(9);
		                    		if(random !=rTemp)  {
		                    			//random = r.nextInt(9);
		                    			rTemp=random;
		                    			break;
		                    		}else{
		                    			continue;
		                    		}
		                    	  }
		                    	   
		                    	   
		                    	   
				            	  updateFocusValue(random);
		                      }
		                  });
		            	
		            	  
		            	 
		          		
		            	  fLayout.setOnClickListener(new OnClickListener() {
		            	  	
		            	  	@Override
		            	  	public void onClick(View arg0) {
		            	  		// TODO Auto-generated method stub
		            	  		
		            	  		if(focusNo == random){

				      				chances1--;
				      				Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
				      				 // Vibrate for 500 milliseconds
				      				 v.vibrate(500);
				      				if(chances1 ==2 ){
				      					ImageView life3 = (ImageView) findViewById(R.id.life3);
				      					life3.setVisibility(life3.GONE);
				      				}	if(chances1 ==1 ){
				      					ImageView life2= (ImageView) findViewById(R.id.life2);
				      					life2.setVisibility(life2.GONE);
				      				}
				      			
		            	  		
		            	  			isClicked = false;
		            	  			
		            	  			if(! (chances1 >0))
			            	  			isThreadRun = true;
		            	  			
		            	  		  fLayout.setBackgroundColor(Color.RED);
		            	  		}else{
		            	  			fLayout.setBackgroundColor(Color.GREEN);
		            	  			 isThreadRun = false;
		            	  		}
		            	  		isClicked = true;
		            	  		
		            	  		
		            	  	}
		            	  });
		            	  
		            	 
		            	  Thread.sleep(sleepTime);
		            	  if(!isClicked && focusNo != random){
		            		  chances1--;
		            		  Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
			      				 // Vibrate for 500 milliseconds
			      				 v.vibrate(500);
		      			
		      				if(! (chances1 >0))
	            	  			isThreadRun = true;
		            	  
		            	  
		            	  }
		              }
		              
		              
		            
		          } catch (InterruptedException e) {
		          }
		      }
		  };

			
			thr.start();
	}
});



		
	}
	
	
	private Runnable updateTimerThread = new Runnable() {

		public void run() {

			
			fLayout.setBackgroundColor(Color.BLACK);
			timeInMilliseconds = SystemClock.uptimeMillis() - startTime;

			updatedTime = timeSwapBuff + timeInMilliseconds;

			int secs = (int) (updatedTime / 1000);
			
			
			if(secs >= 100){
				sleepTime = 750;	
			 }
			else if(secs >= 70){
				sleepTime = 1000;	
			 }
			else if(secs >= 50){
				sleepTime = 1100;	
			 }
			else if(secs >= 30){
				sleepTime = 1200;	
			 }
			else if(secs >= 20){
				sleepTime = 1300;	
			 }
			int mins = secs / 60;
			
//			if(score >= 200){
//				
//				customHandler.removeCallbacks(updateTimerThread);
//				
//				hand1.removeCallbacks(run1);
//				hand2.removeCallbacks(run2);
//				hand3.removeCallbacks(run3);
//				hand4.removeCallbacks(run4);
//				
//				 Intent intent = new Intent(context, MathResultActivity.class);
//				 MathResultActivity.score = "T";
//				 MathResultActivity.scoreV =String.valueOf(score);
//				 MathResultActivity.level=String.valueOf(level);
//				 startActivity(intent);
//				 finish();
//				 return;
//			}
			if(chances1 < 3){
				

  				//chances1--;
  				if(chances1 ==2 ){
  					ImageView life3 = (ImageView) findViewById(R.id.life3);
  					life3.setVisibility(life3.GONE);
  				}	if(chances1 ==1 ){
  					ImageView life2= (ImageView) findViewById(R.id.life2);
  					life2.setVisibility(life2.GONE);
  				}
  				if(chances1 ==0 ){
  					ImageView life1= (ImageView) findViewById(R.id.life1);
  					life1.setVisibility(life1.GONE);
  				}
//				fLayout.setBackgroundColor(Color.RED);
//				  try {
//					Thread.sleep(2000);
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
			
			}
			if(! (chances1 >0)){
				isThreadRun = true;
			customHandler.removeCallbacks(updateTimerThread);
			
			Intent intent  = new Intent(FocusActivity.this,FocusResultActivity.class);     
			intent.putExtra("focusScore", secs);   
			
			startActivity(intent);
			finish();
			return;
			}	
			
		//	secs = secs % 60;
			//int milliseconds = (int) (updatedTime % 1000);
			focusno1.setText("");
			 TextView best = (TextView) findViewById(R.id.highScore);
			 best.setText(String.valueOf(secs));
			customHandler.postDelayed(this, 0);
		}

	};
	
	
	void updateFocusValue(int random){
		
		
		  focus.setText(String.valueOf(random));
		
		  isClicked = false;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

			
	    
			customHandler.removeCallbacks(updateTimerThread);
	        	 Intent intent = new Intent(this, MegaMenuActivity.class);
				 startActivity(intent);
				
				 finish();
	           
	    

	}
	
	
	

}
