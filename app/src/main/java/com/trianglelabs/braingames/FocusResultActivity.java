package com.trianglelabs.braingames;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;

//import com.google.analytics.tracking.android.EasyTracker;
//import com.google.analytics.tracking.android.MapBuilder;
//import com.raghu.brain.braingames.R;
import com.yrkfgo.assxqx4.AdConfig.AdType;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class FocusResultActivity extends Activity {

	
	private Context context ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		context = this;
		
		
		
//		Typeface tf = Typeface.createFromAsset(getAssets(),
//		        "fonts/Plok.ttf");
		setContentView(R.layout.activity_focus_result);
		try{
			AirPushAds.main.showCachedAd(AdType.interstitial);
			}catch(Exception e){
				
			}
		
		TextView	focusR = (TextView) findViewById(R.id.focusResult);
		Intent intent = getIntent();         
		int fScore  = intent.getIntExtra("focusScore", 0);
		//focusR.setTypeface(tf);
		focusR.setText("You could control your focus for "+fScore+ " Seconds");
//		EasyTracker easyTracker  = EasyTracker.getInstance(FocusResultActivity.this);
//
//		 easyTracker.send(MapBuilder.createEvent("focus",
//					String.valueOf(fScore), String.valueOf(fScore), 1l).build());
//
		Map anotherMap = new HashMap();
		try{
			
			

	        FileInputStream fis = openFileInput("math");
	        ObjectInputStream ois = new ObjectInputStream(fis);
	         anotherMap = (Map) ois.readObject();
	        
	         if(null != anotherMap.get("focus") && fScore > (Integer) anotherMap.get("focus")){
	        	 
	        	 anotherMap.put("focus", fScore);
	         }else  if(null == anotherMap.get("focus")){
	        	 anotherMap.put("focus", fScore);
	         }
	         
	         
	        ois.close();
	        ObjectOutputStream oos;
	        FileOutputStream   fos = openFileOutput("math", Context.MODE_PRIVATE);;
			oos = new ObjectOutputStream(fos);
		
			// TODO Auto-generated catch block
		if(null == anotherMap.get("focus"))
			anotherMap.put("focus", fScore);
        oos.writeObject(anotherMap);
        oos.close();
		}catch(Exception e){
			
			FileOutputStream fos;
			try {
				fos = openFileOutput("math", Context.MODE_PRIVATE);;
			
	        ObjectOutputStream oos;
		
				oos = new ObjectOutputStream(fos);
			
				// TODO Auto-generated catch block
			if(null == anotherMap.get("focus"))
				anotherMap.put("focus", fScore);
	        oos.writeObject(anotherMap);
	        oos.close();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				
			}
			
		}
		
		Button playAgain = (Button) findViewById(R.id.playAgain);
		playAgain.setOnClickListener(new  OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				 Intent intent = new Intent(context, FocusActivity.class);
				
				 startActivity(intent);
				 finish();
				
			}
		});
		
		Button menu = (Button) findViewById(R.id.menu);
		menu.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				 Intent intent = new Intent(context, MegaMenuActivity.class);
					
				 startActivity(intent);
				 finish();
			}
		});
		
	   
        
	
	}

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.focus_result, menu);
//		return true;
//	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		 Intent intent = new Intent(context, MegaMenuActivity.class);
			
		 startActivity(intent);
		 finish();
		
	}
}
