package com.trianglelabs.braingames;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.Map;

import android.content.Context;

public class LeaderBoardUtil {

	
	public static int getMultiTaskHighScore(Context context){
		
		int score = 0;
		try {
			FileInputStream mathFis = context.openFileInput("math");
			ObjectInputStream mathois = new ObjectInputStream(mathFis);
			Map mathMap = (Map) mathois.readObject();
		    
			
			int counter =1;
			for(Object key : mathMap.keySet()){
				
				score= score+ Integer.parseInt((String)mathMap.get((String)key+counter));
				counter++;
				
			}
			
			
		} catch (Exception e) {
		}
		return score;
	}
	
	public static int getColorHighScore(){
		
		
		return 0;
	}
	public static int getSearchTaskHighScore(){
		
		
		return 0;
	}
	public static int getMathHighScore(){
		
		
		return 0;
	}
	
public static int getFocusHighScore(){
		
		
		return 0;
	}
}
