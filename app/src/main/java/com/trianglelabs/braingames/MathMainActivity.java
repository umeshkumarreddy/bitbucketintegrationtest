package com.trianglelabs.braingames;

import java.util.Random;

//import com.google.analytics.tracking.android.EasyTracker;
//import com.google.analytics.tracking.android.MapBuilder;
//import com.raghu.brain.braingames.R;
import com.yrkfgo.assxqx4.AdConfig;
import com.yrkfgo.assxqx4.AdView;
import com.yrkfgo.assxqx4.Main;
import com.yrkfgo.assxqx4.AdConfig.AdType;

import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MathMainActivity extends AppCompatActivity {

	TextView que1;
	TextView scoreV;
	TextView ans1;
	TextView que2;
	TextView ans2;
//	Button yes1;
//	Button no1;
//	Button yes2;
//	Button no2;
	Random r= new Random();
	 static boolean isSound = true;
	
	Handler hand1 = new Handler();
	Handler hand2 = new Handler();
	Handler hand3 = new Handler();
	Handler hand4 = new Handler();
	LinearLayout layout1; 
	LinearLayout layout2;

	private TextView timerValue;
	public static int level;
	private long startTime = 0L;
	//private EasyTracker easyTracker = null;
	private Handler customHandler = new Handler();

	long timeInMilliseconds = 0L;
	long timeSwapBuff = 0L;
	long updatedTime = 0L;
	Context context;
	private int score = 0;
	private Main main; //Declare here
	AdView adView;
	//private MediaPlayer mp;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		

   		AdConfig.setAppId(295645);
   		AdConfig.setApiKey("1450682996266650273");
  
   		AdConfig.setCachingEnabled(true);
     
 
	//for calling banner 360
   		//pass the current Activity's reference.
   	
   		setContentView(R.layout.math_activity_main);
   		adView = (AdView) findViewById(R.id.myAdView);
			adView.setBannerType(AdView.BANNER_TYPE_IN_APP_AD);
			adView.setBannerAnimation(AdView.ANIMATION_TYPE_FADE);
			adView.showMRinInApp(false);
			
	   //initialize Airpush
	      main=new Main(this); //Here this is reference of current Activity. 

	      main.startInterstitialAd(AdType.interstitial);
	      AirPushAds.main = main;
		
//		
//		Typeface tf = Typeface.createFromAsset(getAssets(),
//		        "fonts/Plok.ttf");
		
	//	easyTracker = EasyTracker.getInstance(MathMainActivity.this);
		context = this;
		
		if(level == 1){
			levelRange = 10;
			levelRangeMultiply=5;
		}else if(level == 2){
			
			levelRange = 15;
			levelRangeMultiply=8;
		}else if(level == 3){
			
			levelRange = 25;
			levelRangeMultiply=10;
		}else if(level == 4){
			
			levelRange = 40;
			levelRangeMultiply=12;
		}else if(level == 5){
			
			levelRange = 60;
			levelRangeMultiply=15;
		}else if(level == 6){
			
			levelRange = 75;
			levelRangeMultiply=20;
			
		}else if(level == 7){
			
			levelRange = 85;
			levelRangeMultiply=25;
		}else if(level == 8){
			
			levelRange = 100;
			levelRangeMultiply=30;
		}else if(level == 9){
			
			levelRange = 120;
			levelRangeMultiply=35;
			
		}else if(level == 10){
			
			levelRange = 150;
			levelRangeMultiply=40;
		}else if(level == 11){
			
			levelRange = 175;
			levelRangeMultiply=45;
		}
		else if(level == 12){
					
					levelRange = 200;
					levelRangeMultiply=50;
				}
		else if(level == 13){
			
			levelRange = 225;
			levelRangeMultiply=55;
		}else if(level == 14){
			
			levelRange = 250;
			levelRangeMultiply=60;
		}else if(level == 15){
			
			levelRange = 300;
			levelRangeMultiply=70;
		}
//		
//		
//
//		
//		easyTracker.send(MapBuilder.createEvent("MathMain",
//				String.valueOf(level), String.valueOf(level), 1l).build());
		
		timerValue = (TextView) findViewById(R.id.timerValue);
		startTime = SystemClock.uptimeMillis();
		customHandler.postDelayed(updateTimerThread, 0);
//		
//	//	if(isSound){
//	//	mp = MediaPlayer.create(this, R.raw.mathmusic);
//		
//	//	mp.start();
////	}
		scoreV = (TextView) findViewById(R.id.score);
		
		//scoreV.setTypeface(tf);
		scoreV.setText("Score : "+String.valueOf(score));
		que1 = (TextView) findViewById(R.id.question1);
		que2 = (TextView) findViewById(R.id.question2);
		ans1 = (TextView) findViewById(R.id.ans1);
		ans2 = (TextView) findViewById(R.id.ans2);
		layout1 = (LinearLayout) findViewById(R.id.linearLayout1);
		layout2 = (LinearLayout) findViewById(R.id.linearLayout2);
//		yes1 = (Button) findViewById(R.id.yes1);
//		no1 = (Button) findViewById(R.id.no1);
//		yes1 = (Button) findViewById(R.id.yes1);
//		no1 = (Button) findViewById(R.id.no1);
		hand1.postDelayed(run1, 0);
		hand2.postDelayed(run2, 2000);
	}

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.main, menu);
//		return true;
//	}
	boolean tampar = false;
	boolean tampar2 = false;
	private int progressStatus = 0;
	boolean showProgressBar = true;
	private Handler handler = new Handler();
	private ProgressBar progressBar;
	
	private int progressStatus1 = 0;
	boolean showProgressBar1 = true;
	private Handler handlerp = new Handler();
	private ProgressBar progressBar1;
	int chances1 = 3;
	int chances2 = 3;
	private int levelRange;
	private int levelRangeMultiply;
	void showQuestion(){
		
		if(!(chances1 > 0 && chances2 > 0)){
			
			
			if(MathResultActivity.score != null){
				return;
			}
			
			customHandler.removeCallbacks(updateTimerThread);
			
			hand1.removeCallbacks(run1);
			hand2.removeCallbacks(run2);
			hand3.removeCallbacks(run3);
			hand4.removeCallbacks(run4);
			//if(isSound)
		    //	mp.stop();
			 Intent intent = new Intent(context, MathResultActivity.class);
			 MathResultActivity.score = timerValue.getText().toString();
			 MathResultActivity.scoreV = String.valueOf(score);
			 MathResultActivity.level = String.valueOf(level);
			 startActivity(intent);
			 finish();
			 
			return;
		}
		progressStatus= 0;
		showProgressBar = true;
		 progressBar = (ProgressBar) findViewById(R.id.progressBar1);
		
		  // Start long running operation in a background thread
		  new Thread(new Runnable() {
		     public void run() {
		        while (showProgressBar && progressStatus < 100) {
		           progressStatus += 1;
		           
		           if(progressStatus == 99 ){
		        	   tampar = true;
		        	   hand3.postDelayed(run3, 0);
		        	   showProgressBar = false;
		        	   System.out.println("Timeout");
		           }
		    // Update the progress bar and display the 
		                         //current value in the text view
		    handler.post(new Runnable() {
		    public void run() {
		       progressBar.setProgress(progressStatus);
		       
		    }
		        });
		        try {
		           // Sleep for 200 milliseconds. 
		                         //Just to display the progress slowly
		           Thread.sleep(100);
		        } catch (InterruptedException e) {
		           e.printStackTrace();
		        }
		     }
		  }
		  }).start();
		
		int one= r.nextInt(levelRange);
		int two= r.nextInt(levelRange);
		int result =0;
		int plusOrMinus= r.nextInt(3);
		if(plusOrMinus == 0){
		que1.setText(one+" + "+two);
		 result = one+two;
		}
		else if(plusOrMinus == 1){
			
			que1.setText(one+" - "+two);
			 result = one-two;
		}else{
			
			 one= r.nextInt(levelRangeMultiply);
			 two= r.nextInt(levelRangeMultiply);
			 que1.setText(one+" X "+two);
			 result = one*two;
		}
			
		
		
		int random= r.nextInt(2);
		 tampar = false;
		if(random == 0)
		   tampar = true;
		
		if(tampar){
			
			String res =String.valueOf(result);
			if(res.length() > 1 && result > 0){
				 int i =  Integer.parseInt(String.valueOf(res.charAt(0)));
				 i = i-1;
			  res = res.substring(1);
			  res = i+res;
			  result = Integer.parseInt(res);
				
			}else{
				
				result= result-1;
			}
			
		}
		
		ans1.setText(String.valueOf(result)+" ?");
		
		ImageView yes = (ImageView) findViewById(R.id.yes1);
		yes.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// mp.start();
				showProgressBar = false;
				hand3.postDelayed(run3, 0);
				
				hand1.removeCallbacks(run1);
				hand1.postDelayed(run1, 100);
				//showQuestion();
			
			}
		});
		
		ImageView no1 = (ImageView) findViewById(R.id.no1);
		no1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// mp.start();
				showProgressBar = false;
				tampar = !tampar;
				hand3.postDelayed(run3, 0);
				hand1.removeCallbacks(run1);
				hand1.postDelayed(run1, 100);
				//showQuestion();
			
			}
		});
	}
	
	
	void showQuestion2(){
		
		if(!(chances1 > 0 && chances2 > 0)){
			if(MathResultActivity.score != null){
				return;
			}
			customHandler.removeCallbacks(updateTimerThread);
			hand1.removeCallbacks(run1);
			hand2.removeCallbacks(run2);
			hand3.removeCallbacks(run3);
			hand4.removeCallbacks(run4);
			//if(isSound)
			//mp.stop();
			 Intent intent = new Intent(context, MathResultActivity.class);
			 MathResultActivity.score = timerValue.getText().toString();
			 MathResultActivity.scoreV =String.valueOf(score);
			 MathResultActivity.level=String.valueOf(level);
			 startActivity(intent);
			 finish();
		}
		
		progressStatus1= 0;
		showProgressBar1 = true;
		 progressBar1 = (ProgressBar) findViewById(R.id.progressBar2);
		
		  // Start long running operation in a background thread
		  new Thread(new Runnable() {
		     public void run() {
		        while (showProgressBar1 && progressStatus1 < 100) {
		           progressStatus1 += 1;
		           
		           if(progressStatus1 == 99 ){
		        	   tampar2 = true;
		        	   hand4.postDelayed(run4, 0);
		        	   showProgressBar1 = false;
		           }
		    // Update the progress bar and display the 
		                         //current value in the text view
		    handlerp.post(new Runnable() {
		    public void run() {
		       progressBar1.setProgress(progressStatus1);
		       
		    }
		        });
		        try {
		           // Sleep for 200 milliseconds. 
		                         //Just to display the progress slowly
		           Thread.sleep(100);
		        } catch (InterruptedException e) {
		           e.printStackTrace();
		        }
		     }
		  }
		  }).start();
		
		int one= r.nextInt(levelRange);
		int two= r.nextInt(levelRange);
		int result=0;
		int plusOrMinus= r.nextInt(3);
		if(plusOrMinus == 0){
		que2.setText(one+" + "+two);
		 result = one+two;
		}
		else if(plusOrMinus == 1){
			
			que2.setText(one+" - "+two);
			 result = one-two;
		}else{
			
			 one= r.nextInt(levelRangeMultiply);
			 two= r.nextInt(levelRangeMultiply);
			 que2.setText(one+" X "+two);
			 result = one*two;
		}
		
		
		
		int random= r.nextInt(2);
		 tampar2 = false;
		if(random == 0)
		   tampar2 = true;
		
		if(tampar2){
			
			String res =String.valueOf(result);
			if(res.length() > 1 && result > 0){
				 int i =  Integer.parseInt(String.valueOf(res.charAt(0)));
				 i = i-1;
			  res = res.substring(1);
			  res = i+""+res;
			  result = Integer.parseInt(res);
				
			}else{
				
				result= result-1;
			}
			
		}
		
		ans2.setText(String.valueOf(result)+" ?");
		
		ImageView yes = (ImageView) findViewById(R.id.yes2);
		yes.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				showProgressBar1 = false;
				hand4.postDelayed(run4, 0);
				
				hand2.removeCallbacks(run2);
				hand2.postDelayed(run2, 100);
				//showQuestion();
			
			}
		});
		
		ImageView no1 = (ImageView) findViewById(R.id.no2);
		no1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				showProgressBar1 = false;
				tampar2 = !tampar2;
				hand4.postDelayed(run4, 0);
				hand2.removeCallbacks(run2);
				hand2.postDelayed(run2, 100);
				//showQuestion();
			
			}
		});
	}
	
	
	Runnable run1 = new Runnable() {
		@Override
		public void run() {
			layout1.setBackgroundColor(Color.parseColor("#FFFFFF"));
			showQuestion();
			hand1.postDelayed(run1, 10000);
		}
	};
	
	Runnable run2 = new Runnable() {
		@Override
		public void run() {
			layout2.setBackgroundColor(Color.parseColor("#153B4E"));
			showQuestion2();
			hand2.postDelayed(run2, 10000);
		}
	};
	Runnable run3 = new Runnable() {
		@Override
		public void run() {
			if(!tampar)	{
			layout1.setBackgroundColor(Color.GREEN);
			  score = score + 10;
			  scoreV.setText("SCORE : "+String.valueOf(score));
			}else{
				chances1--;
				if(chances1 ==2 ){
					ImageView life3 = (ImageView) findViewById(R.id.life3);
					life3.setVisibility(life3.GONE);
				}	if(chances1 ==1 ){
					ImageView life2= (ImageView) findViewById(R.id.life2);
					life2.setVisibility(life2.GONE);
				}
				
				layout1.setBackgroundColor(Color.RED);
			}
		}
	};
	
	Runnable run4 = new Runnable() {
		@Override
		public void run() {
			if(!tampar2)	{
			layout2.setBackgroundColor(Color.GREEN);
			    score = score + 10;
			    scoreV.setText("SCORE : "+String.valueOf(score));
			}else{
				chances2--;
				if(chances2 ==2 ){
					ImageView life3 = (ImageView) findViewById(R.id.life33);
					life3.setVisibility(life3.GONE);
				}	if(chances2 ==1 ){
					ImageView life2= (ImageView) findViewById(R.id.life22);
					life2.setVisibility(life2.GONE);
				}
				layout2.setBackgroundColor(Color.RED);
			}
		}
	};
	
	private Runnable updateTimerThread = new Runnable() {

		public void run() {

			timeInMilliseconds = SystemClock.uptimeMillis() - startTime;

			updatedTime = timeSwapBuff + timeInMilliseconds;

			int secs = (int) (updatedTime / 1000);
			int mins = secs / 60;
			
			if(secs >= 60){
				
				customHandler.removeCallbacks(updateTimerThread);
				
				hand1.removeCallbacks(run1);
				hand2.removeCallbacks(run2);
				hand3.removeCallbacks(run3);
				hand4.removeCallbacks(run4);
			//	if(isSound)
			//	mp.stop();
				 Intent intent = new Intent(context, MathResultActivity.class);
				 MathResultActivity.score = "T";
				 MathResultActivity.scoreV =String.valueOf(score);
				 MathResultActivity.level=String.valueOf(level);
				 startActivity(intent);
				 finish();
				 return;
			}
			if(!(chances1 > 0 && chances2 > 0)){
				if(MathResultActivity.score != null){
					return;
				}
				customHandler.removeCallbacks(updateTimerThread);
				hand1.removeCallbacks(run1);
				hand2.removeCallbacks(run2);
				hand3.removeCallbacks(run3);
				hand4.removeCallbacks(run4);
				//if(isSound)
				//mp.stop();
				 Intent intent = new Intent(context, MathResultActivity.class);
				 MathResultActivity.score = timerValue.getText().toString();
				 MathResultActivity.scoreV =String.valueOf(score);
				 MathResultActivity.level=String.valueOf(level);
				 startActivity(intent);
				 finish();
			}
//			if(score >= 200){
//				
//				customHandler.removeCallbacks(updateTimerThread);
//				
//				hand1.removeCallbacks(run1);
//				hand2.removeCallbacks(run2);
//				hand3.removeCallbacks(run3);
//				hand4.removeCallbacks(run4);
//				
//				 Intent intent = new Intent(context, MathResultActivity.class);
//				 MathResultActivity.score = "T";
//				 MathResultActivity.scoreV =String.valueOf(score);
//				 MathResultActivity.level=String.valueOf(level);
//				 startActivity(intent);
//				 finish();
//				 return;
//			}
			
			
			secs = secs % 60;
			int milliseconds = (int) (updatedTime % 1000);
			timerValue.setText("" + mins + ":"
					+ String.format("%02d", secs) + ":"
					+ String.format("%03d", milliseconds));
			customHandler.postDelayed(this, 0);
		}

	};
	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub

			
	    AlertDialog alertbox = new AlertDialog.Builder(this)
	    .setMessage("Go back ?")
	    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

	        // do something when the button is clicked
	        public void onClick(DialogInterface arg0, int arg1) {
	        	//MainMenuActivity.interstitial = interstitial;
	        	
	        	customHandler.removeCallbacks(updateTimerThread);
	    		hand1.removeCallbacks(run1);
	    		hand2.removeCallbacks(run2);
	    		hand3.removeCallbacks(run3);
	    		hand4.removeCallbacks(run4);
	        	//if(isSound)
			    	//mp.stop(); 		        	
	        	 Intent intent = new Intent(context, MathMainMenuActivity.class);
				 startActivity(intent);
			    
				 finish();
	           
	            //close();


	        }
	    })
	    .setNegativeButton("No", new DialogInterface.OnClickListener() {

	        // do something when the button is clicked
	        public void onClick(DialogInterface arg0, int arg1) {
	                       }
	    })
	      .show();

	}
	
	
	protected void onStop() {
		
		super.onStop();
		
		customHandler.removeCallbacks(updateTimerThread);
		hand1.removeCallbacks(run1);
		hand2.removeCallbacks(run2);
		hand3.removeCallbacks(run3);
		hand4.removeCallbacks(run4);
		
		
		
		
		
	};
}
