package com.trianglelabs.braingames;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;

//import com.google.analytics.tracking.android.EasyTracker;
//import com.raghu.brain.braingames.R;

import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MathMainMenuActivity extends ActionBarActivity {

	private Context context;
	TextView level1;
	TextView level2;
	TextView level3;
	TextView level4;
	TextView level5;
	TextView level6;
	TextView level7;
	TextView level8;
	TextView level9;
	TextView level10;
	TextView level11;
	TextView level12;
	TextView level13;
	TextView level14;
	TextView level15;
	
//	private EasyTracker easyTracker = null;
	TextView levelT1;
	TextView levelT2;
	TextView levelT3;
	TextView levelT4;
	TextView levelT5;
	TextView levelT6;
	TextView levelT7;
	TextView levelT8;
	TextView levelT9;
	TextView levelT10;
	TextView levelT11;
	TextView levelT12;
	TextView levelT13;
	TextView levelT14;
	TextView levelT15;
	
	ImageView progBar1;
	ImageView progBar2;
	ImageView progBar3;
	ImageView progBar4;
	ImageView progBar5;
	ImageView progBar6;
	ImageView progBar7;
	ImageView progBar8;
	ImageView progBar9;
	ImageView progBar10;
	ImageView progBar11;
	ImageView progBar12;
	ImageView progBar13;
	ImageView progBar14;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.multi_level_selection);
		context = this;
	
		Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);

	      setSupportActionBar(mToolbar);
	      mToolbar.setTitle("Multi Tasking");
	      
//	      getSupportActionBar().setDisplayUseLogoEnabled(true);
//	      getSupportActionBar().setDisplayShowHomeEnabled(true);
//	      getSupportActionBar().setIcon(R.drawable.ic_multi);
	      
		Map anotherMap = new HashMap();
		try{
	
			
			Animation myAnimation =	AnimationUtils.loadAnimation(context, R.anim.bounce);
			
			    level1 = (TextView) findViewById(R.id.level1score);
			    level2 = (TextView) findViewById(R.id.level2score);
			    level3 = (TextView) findViewById(R.id.level3score);
			    level4 = (TextView) findViewById(R.id.level4score);
			    level5 = (TextView) findViewById(R.id.level5score);
			    level6 = (TextView) findViewById(R.id.level6score);
			    level7 = (TextView) findViewById(R.id.level7score);
			    level8 = (TextView) findViewById(R.id.level8score);
			    level9 = (TextView) findViewById(R.id.level9score);
			    level10 = (TextView) findViewById(R.id.level10score);
			    level11 = (TextView) findViewById(R.id.level11score);
			    level12 = (TextView) findViewById(R.id.level12score);
			    level13 = (TextView) findViewById(R.id.level13score);
			    level14 = (TextView) findViewById(R.id.level14score); 
			    level15 = (TextView) findViewById(R.id.level15score);
			    
			    
			    progBar1 = (ImageView) findViewById(R.id.progressBar1);
			    progBar2 = (ImageView) findViewById(R.id.progressBar2);
			    progBar3 = (ImageView) findViewById(R.id.progressBar3);
			    progBar4 = (ImageView) findViewById(R.id.progressBar4);
			    progBar5 = (ImageView) findViewById(R.id.progressBar5);
			    progBar6 = (ImageView) findViewById(R.id.progressBar6);
			    progBar7 = (ImageView) findViewById(R.id.progressBar7);
			    progBar8 = (ImageView) findViewById(R.id.progressBar8);
			    progBar9 = (ImageView) findViewById(R.id.progressBar9);
			    progBar10 = (ImageView) findViewById(R.id.progressBar10);
			    progBar11 = (ImageView) findViewById(R.id.progressBar11);
			    progBar12 = (ImageView) findViewById(R.id.progressBar12);
			    progBar13 = (ImageView) findViewById(R.id.progressBar13);
			    progBar14 = (ImageView) findViewById(R.id.progressBar14);
			    
			    
			    
			    
//			    level1.setTypeface(tf);
//			    level2.setTypeface(tf);
//			    level3.setTypeface(tf);
//			    level4.setTypeface(tf);
//			    level5.setTypeface(tf);
//			    level6.setTypeface(tf);
//			    level7.setTypeface(tf);
//			    level8.setTypeface(tf);
//			    level9.setTypeface(tf);
//			    level10.setTypeface(tf);
			    
			    levelT1 = (TextView) findViewById(R.id.level1Tscore);
			    levelT2 = (TextView) findViewById(R.id.level2Tscore);
			    levelT3 = (TextView) findViewById(R.id.level3Tscore);
			    levelT4 = (TextView) findViewById(R.id.level4Tscore);
			    levelT5 = (TextView) findViewById(R.id.level5Tscore);
			    levelT6 = (TextView) findViewById(R.id.level6Tscore);
			    levelT7 = (TextView) findViewById(R.id.level7Tscore);
			    levelT8 = (TextView) findViewById(R.id.level8Tscore);
			    levelT9 = (TextView) findViewById(R.id.level9Tscore);
			    levelT10 = (TextView) findViewById(R.id.level10Tscore);
			    
			    levelT11 = (TextView) findViewById(R.id.level11Tscore);
			    levelT12 = (TextView) findViewById(R.id.level12Tscore);
			    levelT13 = (TextView) findViewById(R.id.level13Tscore);
			    levelT14 = (TextView) findViewById(R.id.level14Tscore);
			    levelT15 = (TextView) findViewById(R.id.level15Tscore);
			    
//			    levelT1.setTypeface(tf);
//			    levelT2.setTypeface(tf);
//			    levelT3.setTypeface(tf);
//			    levelT4.setTypeface(tf);
//			    levelT5.setTypeface(tf);
//			    levelT6.setTypeface(tf);
//			    levelT7.setTypeface(tf);
//			    levelT8.setTypeface(tf);
//			    levelT9.setTypeface(tf);
//			    levelT10.setTypeface(tf);
			    
			// TextView minfo = (TextView) findViewById(R.id.minfo);
		//	 minfo.setTypeface(tf);
		//	 minfo.startAnimation(myAnimation);
	        FileInputStream fis = openFileInput("math");
	        ObjectInputStream ois = new ObjectInputStream(fis);
	         anotherMap = (Map) ois.readObject();
	        
			if (null != anotherMap.get("level" + 1)) {

				level1.setText("Your Score : "
						+ anotherMap.get("level" + 1).toString());

			}
			if (null != anotherMap.get("level" + 2)) {

				level2.setText("Your Score : "
						+ anotherMap.get("level" + 2).toString());

			}
			if (null != anotherMap.get("level" + 3)) {

				level3.setText("Your Score : "
						+ anotherMap.get("level" + 3).toString());

			}
			if (null != anotherMap.get("level" + 4)) {

				level4.setText("Your Score : "
						+ anotherMap.get("level" + 4).toString());

			}
			if (null != anotherMap.get("level" + 5)) {

				level5.setText("Your Score : "
						+ anotherMap.get("level" + 5).toString());

			}
			if (null != anotherMap.get("level" + 6)) {

				level6.setText("Your Score : "
						+ anotherMap.get("level" + 6).toString());

			}
			if (null != anotherMap.get("level" + 7)) {

				level7.setText("Your Score : "
						+ anotherMap.get("level" + 1).toString());

			}
			if (null != anotherMap.get("level" + 8)) {

				level8.setText("Your Score : "
						+ anotherMap.get("level" + 8).toString());

			}
			if (null != anotherMap.get("level" + 9)) {

				level9.setText("Your Score : "
						+ anotherMap.get("level" + 9).toString());

			}
			if (null != anotherMap.get("level" + 10)) {

				level10.setText("Your Score : "
						+ anotherMap.get("level" + 10).toString());

			}
			if (null != anotherMap.get("level" + 11)) {

				level11.setText("Your Score : "
						+ anotherMap.get("level" + 11).toString());

			}if (null != anotherMap.get("level" + 12)) {

				level12.setText("Your Score : "
						+ anotherMap.get("level" + 12).toString());

			}if (null != anotherMap.get("level" + 13)) {

				level13.setText("Your Score : "
						+ anotherMap.get("level" + 13).toString());

			}if (null != anotherMap.get("level" + 14)) {

				level14.setText("Your Score : "
						+ anotherMap.get("level" + 14).toString());

			}if (null != anotherMap.get("level" + 15)) {

				level15.setText("Your Score : "
						+ anotherMap.get("level" + 15).toString());

			}
			
			
			if (null != anotherMap.get("levelCompleted" + 14)) {
				progBar14.setVisibility(progBar2.VISIBLE);
				progBar1.setVisibility(progBar2.GONE);
			}else if (null != anotherMap.get("levelCompleted" + 13)) {
				progBar13.setVisibility(progBar2.VISIBLE);
				progBar1.setVisibility(progBar2.GONE);
			}else 	if (null != anotherMap.get("levelCompleted" + 12)) {
				progBar12.setVisibility(progBar2.VISIBLE);
				progBar1.setVisibility(progBar2.GONE);
			}else 	if (null != anotherMap.get("levelCompleted" + 11)) {
				progBar11.setVisibility(progBar2.VISIBLE);
				progBar1.setVisibility(progBar2.GONE);
			}else 	if (null != anotherMap.get("levelCompleted" + 10)) {
				progBar10.setVisibility(progBar2.VISIBLE);
				progBar1.setVisibility(progBar2.GONE);
			}else 	if (null != anotherMap.get("levelCompleted" + 9)) {
				progBar9.setVisibility(progBar2.VISIBLE);
				progBar1.setVisibility(progBar2.GONE);
			}else 	if (null != anotherMap.get("levelCompleted" + 8)) {
				progBar8.setVisibility(progBar2.VISIBLE);
				progBar1.setVisibility(progBar2.GONE);
			}else 	if (null != anotherMap.get("levelCompleted" + 7)) {
				progBar7.setVisibility(progBar2.VISIBLE);
				progBar1.setVisibility(progBar2.GONE);
			}else 	if (null != anotherMap.get("levelCompleted" + 6)) {
				progBar6.setVisibility(progBar2.VISIBLE);
				progBar1.setVisibility(progBar2.GONE);
			}else 	if (null != anotherMap.get("levelCompleted" + 5)) {
				progBar5.setVisibility(progBar2.VISIBLE);
				progBar1.setVisibility(progBar2.GONE);
			}else 	if (null != anotherMap.get("levelCompleted" + 4)) {
				progBar4.setVisibility(progBar2.VISIBLE);
				progBar1.setVisibility(progBar2.GONE);
			}else 	if (null != anotherMap.get("levelCompleted" + 3)) {
				progBar3.setVisibility(progBar2.VISIBLE);
				progBar1.setVisibility(progBar2.GONE);
			}else 	if (null != anotherMap.get("levelCompleted" + 2)) {
				progBar2.setVisibility(progBar2.VISIBLE);
				progBar1.setVisibility(progBar2.GONE);
			}
			
			
			
			if (null != anotherMap.get("levelCompleted" + 2)) {
				LinearLayout level1Nc = (LinearLayout) findViewById(R.id.LinearLayout2NonCompleted);
				LinearLayout level1C = (LinearLayout) findViewById(R.id.LinearLayout2Completed);
				level1Nc.setVisibility(level1.GONE);
				level1C.setVisibility(level1.VISIBLE);
			
			}
			if (null != anotherMap.get("levelCompleted" + 3)) {
				LinearLayout level1Nc = (LinearLayout) findViewById(R.id.LinearLayout3NonCompleted);
				LinearLayout level1C = (LinearLayout) findViewById(R.id.LinearLayout3Completed);
				level1Nc.setVisibility(level1.GONE);
				level1C.setVisibility(level1.VISIBLE);
				
			}
			if (null != anotherMap.get("levelCompleted" + 4)) {
				LinearLayout level1Nc = (LinearLayout) findViewById(R.id.LinearLayout4NonCompleted);
				LinearLayout level1C = (LinearLayout) findViewById(R.id.LinearLayout4Completed);
				level1Nc.setVisibility(level1.GONE);
				level1C.setVisibility(level1.VISIBLE);
			
			}
			if (null != anotherMap.get("levelCompleted" + 5)) {
				LinearLayout level1Nc = (LinearLayout) findViewById(R.id.LinearLayout5NonCompleted);
				LinearLayout level1C = (LinearLayout) findViewById(R.id.LinearLayout5Completed);
				level1Nc.setVisibility(level1.GONE);
				level1C.setVisibility(level1.VISIBLE);

			}
			if (null != anotherMap.get("levelCompleted" + 6)) {
				LinearLayout level1Nc = (LinearLayout) findViewById(R.id.LinearLayout6NonCompleted);
				LinearLayout level1C = (LinearLayout) findViewById(R.id.LinearLayout6Completed);
				level1Nc.setVisibility(level1.GONE);
				level1C.setVisibility(level1.VISIBLE);

			}
			if (null != anotherMap.get("levelCompleted" + 7)) {
				LinearLayout level1Nc = (LinearLayout) findViewById(R.id.LinearLayout7NonCompleted);
				LinearLayout level1C = (LinearLayout) findViewById(R.id.LinearLayout7Completed);
				level1Nc.setVisibility(level1.GONE);
				level1C.setVisibility(level1.VISIBLE);

			}
			if (null != anotherMap.get("levelCompleted" + 8)) {
				LinearLayout level1Nc = (LinearLayout) findViewById(R.id.LinearLayout8NonCompleted);
				LinearLayout level1C = (LinearLayout) findViewById(R.id.LinearLayout8Completed);
				level1Nc.setVisibility(level1.GONE);
				level1C.setVisibility(level1.VISIBLE);

			}
			if (null != anotherMap.get("levelCompleted" + 9)) {
				LinearLayout level1Nc = (LinearLayout) findViewById(R.id.LinearLayout9NonCompleted);
				LinearLayout level1C = (LinearLayout) findViewById(R.id.LinearLayout9Completed);
				level1Nc.setVisibility(level1.GONE);
				level1C.setVisibility(level1.VISIBLE);

			}if (null != anotherMap.get("levelCompleted" + 10)) {
				LinearLayout level1Nc = (LinearLayout) findViewById(R.id.LinearLayout10NonCompleted);
				LinearLayout level1C = (LinearLayout) findViewById(R.id.LinearLayout10Completed);
				level1Nc.setVisibility(level1.GONE);
				level1C.setVisibility(level1.VISIBLE);

			} if (null != anotherMap.get("levelCompleted" + 11)) {
				LinearLayout level1Nc = (LinearLayout) findViewById(R.id.LinearLayout11NonCompleted);
				LinearLayout level1C = (LinearLayout) findViewById(R.id.LinearLayout11Completed);
				level1Nc.setVisibility(level1.GONE);
				level1C.setVisibility(level1.VISIBLE);

			} if (null != anotherMap.get("levelCompleted" + 12)) {
				LinearLayout level1Nc = (LinearLayout) findViewById(R.id.LinearLayout12NonCompleted);
				LinearLayout level1C = (LinearLayout) findViewById(R.id.LinearLayout12Completed);
				level1Nc.setVisibility(level1.GONE);
				level1C.setVisibility(level1.VISIBLE);

			} if (null != anotherMap.get("levelCompleted" + 13)) {
				LinearLayout level1Nc = (LinearLayout) findViewById(R.id.LinearLayout13NonCompleted);
				LinearLayout level1C = (LinearLayout) findViewById(R.id.LinearLayout13Completed);
				level1Nc.setVisibility(level1.GONE);
				level1C.setVisibility(level1.VISIBLE);

			} if (null != anotherMap.get("levelCompleted" + 14)) {
				LinearLayout level1Nc = (LinearLayout) findViewById(R.id.LinearLayout14NonCompleted);
				LinearLayout level1C = (LinearLayout) findViewById(R.id.LinearLayout14Completed);
				level1Nc.setVisibility(level1.GONE);
				level1C.setVisibility(level1.VISIBLE);

			}    
			if (null != anotherMap.get("levelCompleted" + 15)) {
				LinearLayout level1Nc = (LinearLayout) findViewById(R.id.LinearLayout15NonCompleted);
				LinearLayout level1C = (LinearLayout) findViewById(R.id.LinearLayout15Completed);
				level1Nc.setVisibility(level1.GONE);
				level1C.setVisibility(level1.VISIBLE);
			}    
	        ois.close();
		}catch(Exception e){
			
			FileOutputStream fos;
			try {
				fos = openFileOutput("math", Context.MODE_PRIVATE);;
			
	        ObjectOutputStream oos;
		
				oos = new ObjectOutputStream(fos);
			
				// TODO Auto-generated catch block
				
			
	        oos.writeObject(anotherMap);
	        oos.close();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				
			}
			
		}
		
		LinearLayout level1 = (LinearLayout) findViewById(R.id.LinearLayout1Completed);
		level1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 Intent intent = new Intent(context, MathMainActivity.class);
				 MathMainActivity.level = 1;
				 startActivity(intent);
				 finish();
			}
		});
		
		LinearLayout level2 = (LinearLayout) findViewById(R.id.LinearLayout2Completed);
		level2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				LinearLayout level1 = (LinearLayout) findViewById(R.id.LinearLayout2Completed);
			     if(level1.getVisibility() == level1.VISIBLE) {  
				 Intent intent = new Intent(context, MathMainActivity.class);
				 MathMainActivity.level = 2;
				 startActivity(intent);
				 finish();
			     }
			}
		});
		LinearLayout level3 = (LinearLayout) findViewById(R.id.LinearLayout3Completed);
		level3.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				LinearLayout level1 = (LinearLayout) findViewById(R.id.LinearLayout3Completed);
			     if(level1.getVisibility() == level1.VISIBLE) {  
				 Intent intent = new Intent(context, MathMainActivity.class);
				 MathMainActivity.level = 3;
				 startActivity(intent);
				 finish();
			     }
			}
		});
		LinearLayout level4 = (LinearLayout) findViewById(R.id.LinearLayout4Completed);
		level4.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				LinearLayout level1 = (LinearLayout) findViewById(R.id.LinearLayout4Completed);
			     if(level1.getVisibility() == level1.VISIBLE) {  
				 Intent intent = new Intent(context, MathMainActivity.class);
				 MathMainActivity.level = 4;
				 startActivity(intent);
				 finish();
			     }
			}
		});
		LinearLayout level5 = (LinearLayout) findViewById(R.id.LinearLayout5Completed);
		level5.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				LinearLayout level1 = (LinearLayout) findViewById(R.id.LinearLayout5Completed);
			     if(level1.getVisibility() == level1.VISIBLE) {  
				 Intent intent = new Intent(context, MathMainActivity.class);
				 MathMainActivity.level = 5;
				 startActivity(intent);
				 finish();
			     }
			}
		});
		LinearLayout level6 = (LinearLayout) findViewById(R.id.LinearLayout6Completed);
		level6.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				LinearLayout level1 = (LinearLayout) findViewById(R.id.LinearLayout6Completed);
			     if(level1.getVisibility() == level1.VISIBLE) {  
				 Intent intent = new Intent(context, MathMainActivity.class);
				 MathMainActivity.level = 6;
				 startActivity(intent);
				 finish();
			     }
			}
		});
		LinearLayout level7 = (LinearLayout) findViewById(R.id.LinearLayout7Completed);
		level7.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				LinearLayout level1 = (LinearLayout) findViewById(R.id.LinearLayout7Completed);
			     if(level1.getVisibility() == level1.VISIBLE) {  
				 Intent intent = new Intent(context, MathMainActivity.class);
				 MathMainActivity.level = 7;
				 startActivity(intent);
				 finish();
			     }
			}
		});
		LinearLayout level8 = (LinearLayout) findViewById(R.id.LinearLayout8Completed);
		level8.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				LinearLayout level1 = (LinearLayout) findViewById(R.id.LinearLayout8Completed);
			     if(level1.getVisibility() == level1.VISIBLE) {  
				 Intent intent = new Intent(context, MathMainActivity.class);
				 MathMainActivity.level = 8;
				 startActivity(intent);
				 finish();
			     }
			}
		});
		LinearLayout level9 = (LinearLayout) findViewById(R.id.LinearLayout9Completed);
		level9.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				LinearLayout level1 = (LinearLayout) findViewById(R.id.LinearLayout9Completed);
			     if(level1.getVisibility() == level1.VISIBLE) {  
				 Intent intent = new Intent(context, MathMainActivity.class);
				 MathMainActivity.level = 9;
				 startActivity(intent);
				 finish();
			     }
			}
		});
		LinearLayout level10 = (LinearLayout) findViewById(R.id.LinearLayout10Completed);
		level10.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				LinearLayout level1 = (LinearLayout) findViewById(R.id.LinearLayout10Completed);
			     if(level1.getVisibility() == level1.VISIBLE) {  
				 Intent intent = new Intent(context, MathMainActivity.class);
				 MathMainActivity.level = 10;
				 startActivity(intent);
				 finish();
			     }
			}
		});
		LinearLayout level11 = (LinearLayout) findViewById(R.id.LinearLayout11Completed);
		level11.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				LinearLayout level1 = (LinearLayout) findViewById(R.id.LinearLayout11Completed);
			     if(level1.getVisibility() == level1.VISIBLE) {  
				 Intent intent = new Intent(context, MathMainActivity.class);
				 MathMainActivity.level = 11;
				 startActivity(intent);
				 finish();
			     }
			}
		});
		LinearLayout level12 = (LinearLayout) findViewById(R.id.LinearLayout12Completed);
		level12.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				LinearLayout level1 = (LinearLayout) findViewById(R.id.LinearLayout12Completed);
			     if(level1.getVisibility() == level1.VISIBLE) {  
				 Intent intent = new Intent(context, MathMainActivity.class);
				 MathMainActivity.level = 12;
				 startActivity(intent);
				 finish();
			     }
			}
		});
		LinearLayout level13 = (LinearLayout) findViewById(R.id.LinearLayout13Completed);
		level13.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				LinearLayout level1 = (LinearLayout) findViewById(R.id.LinearLayout13Completed);
			     if(level1.getVisibility() == level1.VISIBLE) {  
				 Intent intent = new Intent(context, MathMainActivity.class);
				 MathMainActivity.level = 13;
				 startActivity(intent);
				 finish();
			     }
			}
		});
		LinearLayout level14 = (LinearLayout) findViewById(R.id.LinearLayout14Completed);
		level14.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				LinearLayout level1 = (LinearLayout) findViewById(R.id.LinearLayout14Completed);
			     if(level1.getVisibility() == level1.VISIBLE) {  
				 Intent intent = new Intent(context, MathMainActivity.class);
				 MathMainActivity.level = 14;
				 startActivity(intent);
				 finish();
			     }
			}
		});
		LinearLayout level15 = (LinearLayout) findViewById(R.id.LinearLayout15Completed);
		level15.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				LinearLayout level1 = (LinearLayout) findViewById(R.id.LinearLayout15Completed);
			     if(level1.getVisibility() == level1.VISIBLE) {  
				 Intent intent = new Intent(context, MathMainActivity.class);
				 MathMainActivity.level = 15;
				 startActivity(intent);
				 finish();
			     }
			}
		});
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (keyCode == KeyEvent.KEYCODE_BACK) {
	        exitByBackKey();

	        //moveTaskToBack(false);

	        return true;
	    }
	    return super.onKeyDown(keyCode, event);
	}
	protected void exitByBackKey() {
		 Intent intent = new Intent(context, MegaMenuActivity.class);
		 MegaMenuActivity.firstTime = false;
		 startActivity(intent);
		 finish();
	}
}
