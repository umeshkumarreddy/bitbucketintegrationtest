package com.trianglelabs.braingames;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Map;


import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MathResultActivity extends ActionBarActivity {


	public static String score;
	public static String scoreV;
	public static String level;
	//	  private GoogleApiClient mGoogleApiClient;
	Context context;

	//	private EasyTracker easyTracker = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.math_activity_result);

		try {
//		    mGoogleApiClient = new GoogleApiClient.Builder(this)
//		            .addConnectionCallbacks(this)
//		            .addOnConnectionFailedListener(this)
//		            .addApi(Plus.API).addScope(Plus.SCOPE_PLUS_LOGIN)
//		            .addApi(Games.API).addScope(Games.SCOPE_GAMES)
//		            .build();
//
//		    if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
//		         // Log.w(TAG,
//		           //   "GameHelper: client was already connected on onStart()");
//		        } else {
//		          //Log.d(TAG,"Connecting client.");
//		          mGoogleApiClient.connect();
//		        }
//			try{
//			AirPushAds.main.showCachedAd(AdType.interstitial);
//			}catch(Exception e){
//				
//			}
//			easyTracker = EasyTracker.getInstance(MathResultActivity.this);
//
//			easyTracker.send(MapBuilder.createEvent("MathResult",
//					scoreV, level, 1l).build());

//			Typeface tf = Typeface.createFromAsset(getAssets(),
//			        "fonts/Kleymissky_0283.otf");

			FileInputStream fis = openFileInput("math");
			ObjectInputStream ois = new ObjectInputStream(fis);
			Map anotherMap = (Map) ois.readObject();

			anotherMap.put("level" + level, scoreV);

			ois.close();
			int sco = Integer.parseInt(scoreV);
			context = this;
			TextView timerValue = (TextView) findViewById(R.id.res);
			//	timerValue.setTypeface(tf);
			String res = "";
			ImageView win = (ImageView) findViewById(R.id.win);
			ImageView gameOver = (ImageView) findViewById(R.id.gameOver);
			if (sco >= 200) {

				win.setVisibility(win.VISIBLE);
				gameOver.setVisibility(gameOver.GONE);
				Button pAgain = (Button) findViewById(R.id.pAgain);


				if (Integer.parseInt(level) >= 15) {

					pAgain.setVisibility(pAgain.GONE);
					res = " Your Score : " + sco + "\n Congratulations...  you are the math champian. Be proud of your math skills";

				} else {

					res = "          Your Score : " + sco + "\n  You finised this level. Next level has been unlocked.";
					anotherMap.put("levelCompleted" + (Integer.parseInt(level) + 1), "Y");


					pAgain.setText("Level " + (Integer.parseInt(level) + 1));
					pAgain.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View arg0) {
							// TODO Auto-generated method stub
							MathMainActivity.level = (Integer.parseInt(level) + 1);
							Intent intent = new Intent(context, MathMainActivity.class);
							score = null;
							startActivity(intent);
							finish();
						}
					});


				}
			} else {
				win.setVisibility(win.GONE);
				gameOver.setVisibility(gameOver.VISIBLE);

				res = "        Your Score : " + sco + "\n You could not reach target score of this level. Try again. All the best.";

				Button pAgain = (Button) findViewById(R.id.pAgain);
				pAgain.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						Intent intent = new Intent(context, MathMainActivity.class);
						score = null;
						startActivity(intent);
						finish();
					}
				});

			}

			timerValue.setText(res);


			Button menu = (Button) findViewById(R.id.menu);
			menu.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// mGoogleApiClient.connect();
//				Games.Leaderboards.submitScore(mGoogleApiClient, "CgkIxvHP8MUYEAIQAQ", LeaderBoardUtil.getMathHighScore());
//            	startActivityForResult(Games.Leaderboards.getLeaderboardIntent(mGoogleApiClient,
//            	        "CgkIxvHP8MUYEAIQAQ"), 2);

					// TODO Auto-generated method stub
					Intent intent = new Intent(context, MathMainMenuActivity.class);
					score = null;
					startActivity(intent);
					finish();
				}
			});

			FileOutputStream fos = openFileOutput("math", Context.MODE_PRIVATE);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(anotherMap);
			oos.close();

		} catch (Exception e) {

			e.printStackTrace();
		}


	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		try {
			super.onBackPressed();
			Intent intent = new Intent(context, MathMainMenuActivity.class);
			score = null;
			startActivity(intent);

			finish();
		} catch (Exception e) {
		}
	}


}
