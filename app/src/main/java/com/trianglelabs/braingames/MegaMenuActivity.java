package com.trianglelabs.braingames;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;

//import com.google.analytics.tracking.android.EasyTracker;
//import com.google.analytics.tracking.android.MapBuilder;
//import com.raghu.brain.braingames.R;
import com.yrkfgo.assxqx4.AdConfig;
import com.yrkfgo.assxqx4.AdConfig.AdType;
import com.yrkfgo.assxqx4.AdView;
import com.yrkfgo.assxqx4.Main;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MegaMenuActivity extends ActionBarActivity {

	//private EasyTracker easyTracker = null;
	// ImageView sound;
	// ImageView mute;
	private Toolbar mToolbar;
	static boolean isSound = true;
	static boolean firstTime = true;
	private Context context;
	Map anotherMap = new HashMap();

	private Main main2; // Declare here
	AdView adView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		AdConfig.setAppId(295645);
   		AdConfig.setApiKey("1450682996266650273");
  
  		AdConfig.setCachingEnabled(true);
   		
		setContentView(R.layout.activity_ball_math_menu);
		context = this;
		adView = (AdView) findViewById(R.id.myAdView);
		adView.setBannerType(AdView.BANNER_TYPE_IN_APP_AD);
		adView.setBannerAnimation(AdView.ANIMATION_TYPE_FADE);
		adView.showMRinInApp(false);
		
   //initialize Airpush
      main2=new Main(this); //Here this is reference of current Activity. 

      main2.startInterstitialAd(AdType.interstitial);
      
     
      AirPushAds.main2 = main2;
		// for calling banner 360
		// pass the current Activity's reference.
		setContentView(R.layout.activity_mega_menu);
		Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(mToolbar);

		// Typeface tf = Typeface.createFromAsset(getAssets(),
		// "fonts/Plok.ttf");
		if (firstTime) {

			int recency = 0;
			try {
				//easyTracker = EasyTracker.getInstance(MegaMenuActivity.this);

				FileInputStream fis = openFileInput("math");
				ObjectInputStream ois = new ObjectInputStream(fis);
				anotherMap = (Map) ois.readObject();

				recency = (Integer) anotherMap.get("recency");

				if (recency > 3 && !"Y".equalsIgnoreCase((String) anotherMap.get("Rated"))) {

					AlertDialog alertDialog = new AlertDialog.Builder(this).create();

					alertDialog.setTitle("Rate App");

					alertDialog.setMessage("Liked App? Please Rate 5 star");

					alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int id) {

							anotherMap.put("Rated", "Y");
							try {
								FileOutputStream fos;

								fos = openFileOutput("math", Context.MODE_PRIVATE);
								;

								ObjectOutputStream oos;

								oos = new ObjectOutputStream(fos);

								// TODO Auto-generated catch block

								oos.writeObject(anotherMap);
								oos.close();

							} catch (Exception e) {
							}

						//	easyTracker.send(MapBuilder.createEvent("Rating", "Yes", "Yes", 1l).build());
							context.startActivity(new Intent(Intent.ACTION_VIEW,
									Uri.parse("market://details?id=com.raghu.braingame")));

						}
					});

					alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Not Now",
							new DialogInterface.OnClickListener() {

								public void onClick(DialogInterface dialog, int id) {

//									easyTracker
//											.send(MapBuilder.createEvent("Rating", "Not Now", "Not Now", 1l).build());

								}
							});

					alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Never", new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int id) {
					//		easyTracker.send(MapBuilder.createEvent("Rating", "Never", "Never", 1l).build());
							anotherMap.put("Rated", "Y");
							try {
								FileOutputStream fos;

								fos = openFileOutput("math", Context.MODE_PRIVATE);
								;

								ObjectOutputStream oos;

								oos = new ObjectOutputStream(fos);

								// TODO Auto-generated catch block

								oos.writeObject(anotherMap);
								oos.close();

							} catch (Exception e) {
							}

						}
					});
					alertDialog.show();
				}
				recency = recency + 1;
				FileOutputStream fos;

				fos = openFileOutput("math", Context.MODE_PRIVATE);
				;

				ObjectOutputStream oos;

				oos = new ObjectOutputStream(fos);

				// TODO Auto-generated catch block

				anotherMap.put("recency", recency);
				oos.writeObject(anotherMap);
				oos.close();

				ois.close();
			} catch (Exception e) {

				FileOutputStream fos;
				try {
					fos = openFileOutput("math", Context.MODE_PRIVATE);
					;

					ObjectOutputStream oos;

					oos = new ObjectOutputStream(fos);

					// TODO Auto-generated catch block
					recency = recency + 1;
					anotherMap.put("recency", recency);
					oos.writeObject(anotherMap);
					oos.close();
				} catch (Exception e1) {

					// TODO Auto-generated catch block

				}

			}
		}
		// sound = (ImageView) findViewById(R.id.sound);
		// mute = (ImageView) findViewById(R.id.mute);
		// sound.setAnimation(AnimationUtils.loadAnimation(this,
		// android.R.anim.slide_in_left));
		// if(isSound){
		//
		// sound.setVisibility(sound.VISIBLE);
		// mute.setVisibility(sound.GONE);
		// }else{
		// sound.setVisibility(sound.GONE);
		// mute.setVisibility(sound.VISIBLE);
		// }
		//
		// sound.setOnClickListener(new OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// // TODO Auto-generated method stub
		// sound.setVisibility(sound.GONE);
		// mute.setVisibility(mute.VISIBLE);
		// isSound = false;
		// //mp.reset();
		//
		// }
		// });
		//
		// mute.setOnClickListener(new OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// // TODO Auto-generated method stub
		// mute.setVisibility(sound.GONE);
		// sound.setVisibility(sound.VISIBLE);
		// isSound = true;
		// }
		// });
		//
		context = this;

		try {
			FileInputStream mathFis = openFileInput("math");
			ObjectInputStream mathois = new ObjectInputStream(mathFis);
			Map mathMap = (Map) mathois.readObject();
			int mathCompletedLevel = getCompletedLevel(mathMap);
			TextView multiPercentageCompletion = (TextView) findViewById(R.id.multiPercentageCompletion);

			multiPercentageCompletion.setText(Math.round(mathCompletedLevel * 100 / 15f) + "% Completed");
		} catch (Exception e) {
		}

		try {
			FileInputStream ballMathFis = openFileInput("ballmath");
			ObjectInputStream ballMathois = new ObjectInputStream(ballMathFis);
			Map ballMathMap = (Map) ballMathois.readObject();
			int ballMathCompletedLevel = getCompletedLevel(ballMathMap);
			TextView mathPercentageCompletion = (TextView) findViewById(R.id.mathPercentageCompletion);

			mathPercentageCompletion.setText(Math.round(ballMathCompletedLevel * 100 / 15f) + "% Completed");
		} catch (Exception e) {
		}

		try {
			FileInputStream reverseFis = openFileInput("reverse");
			ObjectInputStream reverseOis = new ObjectInputStream(reverseFis);
			Map reverseMap = (Map) reverseOis.readObject();
			int reverseCompletedLevel = getCompletedLevel(reverseMap);

			TextView searchPercentageCompletion = (TextView) findViewById(R.id.searchPercentageCompletion);

			searchPercentageCompletion.setText(Math.round(reverseCompletedLevel * 100 / 15f) + "% Completed");

		} catch (Exception e) {
		}
		
		try {
			
			FileInputStream mathFis = openFileInput("math");
			ObjectInputStream mathois = new ObjectInputStream(mathFis);
			Map mathMap = (Map) mathois.readObject();
			 if(null != mathMap.get("focus") ){
		    	
			TextView focusBest = (TextView) findViewById(R.id.focusBest);

			focusBest.setText("BEST:"+mathMap.get("focus") +" Secs");;
			 }
		} catch (Exception e) {
		}
		
	try {
			
			FileInputStream mathFis = openFileInput("math");
			ObjectInputStream mathois = new ObjectInputStream(mathFis);
			Map mathMap = (Map) mathois.readObject();
			 if(null != mathMap.get("color") ){
		    	
			TextView colorBest = (TextView) findViewById(R.id.colorBest);
			int colorScore = ((Integer)mathMap.get("color"))*10;
			colorBest.setText("BEST:"+colorScore);
			 }
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		

		LinearLayout mathc = (LinearLayout) findViewById(R.id.mathc);
		// mathc.setTypeface(tf);
		mathc.setAnimation(AnimationUtils.loadAnimation(this, android.R.anim.slide_in_left));
		mathc.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, MathMainMenuActivity.class);
				MathMainActivity.isSound = isSound;
				startActivity(intent);

				// overridePendingTransition(R.anim.rotate_out,R.anim.rotate_in);
				finish();
				return;
			}
		});

		LinearLayout focus = (LinearLayout) findViewById(R.id.focus_linear);
		// focus.setTypeface(tf);
		focus.setAnimation(AnimationUtils.loadAnimation(this, android.R.anim.slide_in_left));
		focus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, FocusActivity.class);

				startActivity(intent);
				finish();
				return;
			}
		});
		
		LinearLayout color = (LinearLayout) findViewById(R.id.color);
		// focus.setTypeface(tf);
		color.setAnimation(AnimationUtils.loadAnimation(this, android.R.anim.slide_in_left));
		color.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, ColorMainActivity.class);

				startActivity(intent);
				finish();
				return;
			}
		});

		LinearLayout goReverse = (LinearLayout) findViewById(R.id.goReverse);
		// goReverse.setTypeface(tf);
		goReverse.setAnimation(AnimationUtils.loadAnimation(this, android.R.anim.slide_in_left));
		goReverse.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, GoReverseLevelSeceletionActivity.class);

				startActivity(intent);

				finish();
				return;
			}
		});

		LinearLayout ballMath = (LinearLayout) findViewById(R.id.ballMath);
		// ballMath.setTypeface(tf);
		ballMath.setAnimation(AnimationUtils.loadAnimation(this, android.R.anim.slide_in_left));
		ballMath.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(context, BallMathMenuActivity.class);

				startActivity(intent);
				finish();
				return;
			}
		});
	}

	private int getCompletedLevel(Map anotherMap) {

		if (null != anotherMap.get("levelCompleted" + 14)) {
			return 14;
		} else if (null != anotherMap.get("levelCompleted" + 13)) {
			return 13;
		} else if (null != anotherMap.get("levelCompleted" + 12)) {
			return 12;
		} else if (null != anotherMap.get("levelCompleted" + 11)) {
			return 11;
		} else if (null != anotherMap.get("levelCompleted" + 10)) {
			return 10;
		} else if (null != anotherMap.get("levelCompleted" + 9)) {
			return 9;
		} else if (null != anotherMap.get("levelCompleted" + 8)) {
			return 8;
		} else if (null != anotherMap.get("levelCompleted" + 7)) {
			return 7;
		} else if (null != anotherMap.get("levelCompleted" + 6)) {
			return 6;
		} else if (null != anotherMap.get("levelCompleted" + 5)) {
			return 5;
		} else if (null != anotherMap.get("levelCompleted" + 4)) {
			return 4;
		} else if (null != anotherMap.get("levelCompleted" + 3)) {
			return 3;
		} else if (null != anotherMap.get("levelCompleted" + 2)) {
			return 2;
		} else if (null != anotherMap.get("levelCompleted" + 1)) {
			return 1;
		} else {
			return 0;
		}
	}

	protected void exitByBackKey() {

		AlertDialog alertbox = new AlertDialog.Builder(this).setMessage("Quit Game ?")
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

					// do something when the button is clicked
					public void onClick(DialogInterface arg0, int arg1) {

						MegaMenuActivity.this.finish();
						System.exit(0);
						// close();

					}
				}).setNegativeButton("No", new DialogInterface.OnClickListener() {

					// do something when the button is clicked
					public void onClick(DialogInterface arg0, int arg1) {
					}
				}).show();

	}


	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		try {
			AirPushAds.main2.showCachedAd(AdType.interstitial);
        } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public boolean onKeyDown(final int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK) {
			//Ask the user if they want to quit
			new AlertDialog.Builder(this)
					.setIcon(android.R.drawable.ic_dialog_alert)
					.setTitle("Quit Brain Games")
					.setMessage("Are You Sure ?")
					.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {

							//Stop the activity
							finish();
							System.exit(keyCode);

						}

					})
					.setNegativeButton("No", null)
					.show();

			return true;
		}
		else {
			return super.onKeyDown(keyCode, event);
		}

	}


}
